/**
 * Server instantiation
 * @namespace app
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 */

var env = require('./config/dev_env');
var mongoose = require('./config/mongoose');
var express = require('./config/express');
var db = mongoose();
var app = express();

app.listen(env.port);
module.exports = app;