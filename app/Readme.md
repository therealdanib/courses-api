Descripción
-----------
**Authentication Api** 
Esta API incorpora apis y usuarios de Apithy en un ecosistema único de aplicaciones, permitiendo que nuevos
desarrollos se incorporen a este ecosistema de manera simple y transparente.

Requerimientos
--------------
Esta API esta desarrollada para ser accesada desde cualquier parte, sin importar la tecnología que el cliente tenga.
Cómo requerimientos, solo se requiere que el cliente pueda hacer peticiones con los verbos [HTTP][1].

Cada verbo ejecuta una acción definida:

> - **POST** 
    > ***Crea un objeto***
> - **GET** 
    > ***Obtiene uno o más documentos|objetos***
> - **PUT** 
    > ***Modifica un documento|objeto***
> - **DELETE** 
    > ***Elimina un documento|objeto***

Así cómo que pueda procesar información en formato [JSON][2]

Conectividad
------------
Para establecer una comunicación con el API, es necesario proporcionar un identificador de aplicación en formato [UUID][3]
este Identificador deberá ser enviado en conjunto con un Password que le habrá sido proporcionado al equipo de desarrollo 
de la aplicación. Este password, deberá ser envíado codificado en [Base64][4]. 

Una vez validado el UUID y password de la aplicación, se devolvera un Token, mismo que deberá ser incluido en cada 
petición como parte de los headers. Bajo un header personalizado llamado: **X-Access-Token** o **X-Key**.

Todas las peticiones deben de ser enviada con formato application/json.

----------

Tecnología
----------
Este API, esta desarrollada e implementada con las siguientes tecnologías.

* [Node.js][6] 
* [MongoDB][5]

----------

Autenticación
-------------

Para poder hacer uso del API, es necesario contar con el identificador de la aplicación y el password para autenticar la 
misma. De esta manera, solo las aplicaciones registradas y autorizadas, pueden hacer uso de la misma.

**Petición**
    
    POST /getAuthorization HTTP/1.1
    Host: localhost:3000 | courses.apithy.com
    Content-Type: application/json
    Cache-Control: no-cache
    
    {"idapplication": "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx", "passwordapp": "RGVtb0Rhcndpbg=="}
    
**Respuesta**
    
    Content-Type: application/json; charset=utf-8
    Content-Length: 275
    Status: 200
    
    {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NDY3NDY4OTcyNjQsImlkQXBwIjoieHh4eHh4eHgteHh4eC1NeHh4LU54eHgteHh4eHh4eHh4eHh4Iiwicm9sZSI6ImFkbWluIn0.y5qgRskG-Mk65j7dWcsrIqMJU2hr77jV_5r4LnNTiQM",
        "expires": 1446746897264,
        "publicId": "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx",
        "name": "Demo Application",
        "idApplication": "XxXxXxXxXxXxXxXxXxXxXxXx"
    }

----------


Códigos de Respuesta
--------------------

El API devuelve los siguientes códigos de respuesta mismos que permiten identificar el resultado de la petición.

| **Código** | **Mensaje** | **¿Cúando sucede?** |
|:---:|:---|:---|
| **200** | Successful HTTP request | La petición es correcta y devuelve los datos esperados|
| **400** | Bad Request. Please review API Reference | Invalid ID, errores en tipo de datos|
| **401** | Authorization failed | El token no es valido, ha expirado o no fue enviado |
| **403** | Endpoint not allowed | Carece de los permisos para acceder a este endpoint |
| **404** | Resource not found | El recurso solicitado no existe |
| **409** | Update conflict. Update cancelled| Los datos que se enviaron para la actualización generán un conflicto con otros datos.|
| **500** | Internal error. Operation has failed | Errores internos del servidor o de la base de datos al guardar elementos incompletos o erroneos |

----------


Ejemplos de respuesta
---------------------

**Error 401**

    Content-Type: application/json; charset=utf-8
    Content-Length: 47
    Status: 401
    {
      "status": 401,
      "message": "Authorization failed"
    }
    
----------


Documentación
-------------

Esta documentación esta generada mediante [JSDoc][7]


[1]: http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
[2]: http://www.json.org/
[3]: https://es.wikipedia.org/wiki/Identificador_%C3%BAnico_universal
[4]: https://es.wikipedia.org/wiki/Base64
[5]: https://www.mongodb.com
[6]: https://nodejs.org
[7]: http://usejsdoc.org/
