/**
 *
 * Information security and token validation utilities
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/config/security
 *
 */

var jwt  = require('jwt-simple');
var encryption = require('./encryption');
var utils = require('./utils');
var applications = require('../models/application');

var security = {

    /**
     * This function creates a new token based on given application data
     *
     *
     * @function generateToken
     * @param application {object} Application object
     * @param days {Number} Days to expiration date
     * @returns {{token: String, expires: Date}} Returns a token including expiration date, and the identifier of the application in turn
     *
     */

    generateToken: function(application, days){

        var expirationDate = new Date();
        expirationDate.setDate(expirationDate.getDate()+days);


        var token = jwt.encode({

            expiring: expirationDate,
            applicationId: application.publicId

        }, encryption.getSecret(), 'HS512');

        return ({

            token: token,
            expires: expirationDate,
            id: application._id
        });
    },

    /**
     *
     * Function that validates that an application is registered and active.
     *
     * @private
     * @function validateApplication
     * @param applicationId {ObjectId} Application's public id
     * @param applicationPassword {String} Application password
     * @param callback Callback that handles function response
     */

    validateApplication: function(applicationId, applicationPassword, callback){

        var passwd = utils.base64Decode(applicationPassword);

        applications.findOne({'publicId': applicationId, 'active': true}, function (err, app){

            if(!err && app !== null && encryption.decrypt(app.password) === passwd){

                callback(app);
            }
            else{

                callback(false);
            }
        });
    },

    /**
     *
     * This function is accessible via API for an application to get proper credentials in order to access this API's data.
     *
     * @function getCredentials
     * @param request {object} Request object
     * @param response {object} Response object
     * @example
     * POST / HTTP 1.1
     * /getAuthorization
     * Raw:{
     *  'applicationId':'832328-sadusaduas-2172821',
     *  'applicationPassword': 'UA8A83849A384A3424942='
     * }
     */

    getCredentials: function(request,response){

        var applicationId = request.body.applicationId || '';
        var applicationPassword = request.body.applicationPassword || '';

        if(applicationId === '' || applicationPassword === ''){

            response.status(400);
            response.json(utils.getResponseFormat(400,'Application data is missing', null));
            return;
        }

        security.validateApplication(applicationId, applicationPassword, function(app){

            if(!app){

                response.status(401);
                response.json(utils.getResponseFormat(401,"Wrong application id or password",null));
            }
            else{

                response.json(security.generateToken(app, 30));
            }
        });
    },

    /**
     *
     * Function that validates JWT fields and resolves if an application is granted to access API's data
     * @function authenticate
     *
     * @param request Request
     * @param callback Callback that handles function response
     */

    authenticate: function(request, callback){

        var token = request.headers['x-access-token'];
        var jwtoken = security.decodeToken(token);

        if(jwtoken['error']){

            callback(jwtoken);
            return;
        }

        applications.findOne({'publicId': jwtoken.applicationId, 'active': true}, function (err, app) {

            if(err || !app){

                callback(utils.getResponseFormat(401,"Application is not active",null));
            }
            else{

                if (new Date(jwtoken.expiring) < Date.now()){

                    callback(utils.getResponseFormat(401,'Token has expired',null));
                }

                else callback(true);
            }
        });
    },

    /**
     *
     * Function that decodes JWT
     *
     * @function decodeToken
     * @param token Token to be decoded
     * @returns {*} Returns decoded JWT if success or error trace if not
     */

    decodeToken: function(token){

        try{

            return jwt.decode(token, encryption.getSecret());

        }catch(err){

            return utils.getResponseFormat(401,err.toString(),null);
        }
    },

    /**
     *
     * Middleware that handles access to API's data via JWT validation. This is invoked via each module's router
     *
     * @function grantAccess
     * @param request {object} Request object
     * @param response {object} Response object
     * @param next {object} Cursor to get the next function
     */

    grantAccess: function (request,response,next){

        security.authenticate(request,function(result){

            if(result !== true){

                response.status(result.status);
                response.json(result);

            } else{

                next();
            }
        });
    }
};

module.exports = security;