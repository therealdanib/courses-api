/**
 *  Application environment configuration variables
 *
 *  @author Julio Bravo <julio@geekstadium.com>
 *  @version 0.1.0
 *  @module app/config/env
 */

var port = 3001;
module.exports = {
    port: port,
    db: 'mongodb://127.0.0.1:27017/course-Apithy',
    versionUrl: '/api/v1/'
};