/**
 * Encoding,requests and responses utilities.
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 1.0.0.
 * @module config/utils
 */

// This is where i handle response messages according to a status number given.

var Path = require('../models/path');
var Course = require('../models/course');
var statusEnum = {
    200: "Successfully performed",
    400: "Bad Request. Please review API Reference",
    401: "Authorization failed",
    403: "Endpoint not allowed",
    404: "Resource not found",
    405: "Method not allowed",
    409: "Update conflict. Update cancelled",
    500: "Internal error. Operation has failed"
};

var utils = {

    /**
     *
     * Middleware for response handling according to W3C specs
     *
     * @function getResponseFormat
     * @param status {Number} Response status
     * @param error {object} Error description (Could be internal, or database generated)
     * @param id {string} Object identifier (Could be also an object with detailed information)
     * @returns {{status: *, message: *, error: string}} Returns an object with status and message as fields. It might also include error or id fields.
     */

    getResponseFormat: function (status, error, id) {

        return {

            'status': status,
            'message': statusEnum[status],
            'error': (error != null ? error : undefined),
            'id': (id != null ? id : undefined)
        };
    },

    /**
     *
     * Mongoose error handler
     *
     * @function handleDBError
     * @param error {object} MongoDB error object
     * @param response {object} Response object
     * @version 1.0.0
     */

    handleDBError: function(error,response){

        response.status(400);
        response.json(utils.getResponseFormat(400,error,null));
    },

    /**
     *
     * Middleware for handling unregistered methods in a route
     *
     * @function handleNotAllowed
     *
     * @param request {object} Request object
     * @param response {object} Response object
     */

    handleNotAllowed: function (request,response){

        response.status(405);
        response.json(utils.getResponseFormat(405,null,null));
    },


    /**
     *
     * Middleware for handling single object updates or insertions
     *
     * @function handleUpsertResponse
     * @param err {object} Error de la BD
     * @param doc {object} Resultado del query
     * @param res {object} Respuesta http
     */

    handleUpsertResponse: function(err,doc,res){

        if (err) {

            utils.handleDBError(err,res);
        }
        else {

            if(!doc || !doc._id){
                
                res.status(404);
                res.json(utils.getResponseFormat(404,'Nothing found'));
            }
            else{

                res.status(200);
                res.json(utils.getResponseFormat(200,null,doc._id));
            }
        }
    },

    /**
     *
     * Middleware for handling batch responses
     *
     * @function handleBatchResponse
     * @param err {object} DB Error object
     * @param docs {array} Query result
     * @param res {object} Http response
     */

    handleBatchResponse: function(err,docs,res){

        if(err){

            utils.handleDBError(err,res);
        }
        else{
            if(!docs || docs.length === 0){
                res.status(404);
                res.json(utils.getResponseFormat(404,'Nothing found'));
            }
            else{
                res.status(200);
                res.json(docs);
            }
        }
    },

    /**
     *
     * Middleware for handling single object responses
     *
     * @function handleSingleResponse
     * @param err {object} DB Error
     * @param doc {array} Query result
     * @param res {object} Http response
     *
     */

    handleSingleResponse: function(err,doc,res){

        if(err){

            utils.handleDBError(err,res);
        }
        else{

            if(!doc){

                res.status(404);
                res.json(utils.getResponseFormat(404,'Nothing found'));
            }
            else{

                res.status(200);
                res.json(doc);
            }
        }
    },

    handleBulkOpResponse: function(err,docs,res){

        if(err){

            utils.handleDBError(err,res);
        }
        else{

            res.status(200);
            res.json({
                'status': 200,
                'message': 'Bulk operation performed successfully'
            });
        }
    },

    /**
     *
     * Function for Base64 to ASCII decoding
     *
     * @function base64Decode
     * @param text {string} String to decode
     * @returns {string} Decoded string
     * @see https://es.wikipedia.org/wiki/ASCII
     */

    base64Decode: function(text){

        return (text === '' || typeof text === 'undefined' ? '' : new Buffer(text, 'base64').toString('ascii'));
    },

    /**
     *
     * Gets an average value from a given key field in an array
     *
     * @function getAvg
     * @param arrayElems {array} Array containing elements
     * @param key {String} Field/Key name
     * @returns {number} Average value
     */
    
    getAvg: function(arrayElems, key){

        if(!arrayElems.length){
            return 0;
        }

        return arrayElems.map(function (item) {
                return item[key];
            }).reduce(function(a, b){return a + b}, 0)/arrayElems.length;
    },

    /**
     *
     * Updates a preview path/course for a company
     *
     * @function updatePreview
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/path/preview/574df8eb86b676540408cf0a/574df8eb86b676540408cf1c
     * /api/v1/course/preview/574df8eb86b676540408cf0a/574df8eb86b676540408cf1c
     * Raw:
     * {
     *  "format": "image",
     *  "url": "432747273432/32372732723/283329-382832332-9329392.png"
     * }
     */

    updatePreview: function(req,res){

        var _id = req.params.modelId;
        var com = req.params.comId;
        var model = req.params.objModel.toLowerCase() === 'path' ? Path : req.params.objModel.toLowerCase() === 'course' ? Course : undefined;
        var previewUpd = req.body;

        if(model === undefined){

            res.status(404);
            res.json(utils.getResponseFormat(404,'Model not found'));
            return;
        }

        model.findOneAndUpdate({_id: _id, _companyId: com}, previewUpd, {new: true}, function(err,doc){
            utils.handleUpsertResponse(err,doc,res);
        });
    }
    
};

module.exports = utils;