/**
 * Express dependencies configuration
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module config/express
 *
 */

var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var utils = require('./utils');

/**
 * Function that catches all configuration items to build and return an express application object
 *
 * @function exports
 * @returns {object} Returns an express application object
 */

module.exports = function(){

    var app = express();
    app.use(morgan('dev'));

    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    function setupCORS(req, res, next) {
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-type,Accept,X-Access-Token,X-Key');
        res.header('Access-Control-Allow-Origin', '*');
        if (req.method === 'OPTIONS') {
            res.status(200).end();
        } else {
            next();
        }
    }

    app.all('/*', setupCORS);

    require('../routes/common')(app);
    require('../routes/security')(app);
    require('../routes/course')(app);
    require('../routes/sessionType')(app);
    require('../routes/path')(app);
    require('../routes/enrollment')(app);
    require('../routes/application')(app);
    require('../routes/stats/enrollment')(app);

    app.use(express.static('public'));

    function returnInvalidJSON(err, req, res, next) {

        if (err) {

            res.status(400);
            res.json(utils.getResponseFormat(400,err.toString(),null));

        } else {

            next();
        }
    }

    function return404(req, res){

        res.status(404);
        res.json(utils.getResponseFormat(404,'Endpoint not found',null));
    }

    app.use(returnInvalidJSON);
    app.use(return404);

    return app;
};