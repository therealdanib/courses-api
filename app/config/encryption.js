/**
 * Encryption utilities
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 1.0.0.
 * @module app/config/encryption
 */
var crypto = require('crypto');

var encryption = {

    /**
     *
     * Function that encrypts a text string and then returns it
     *
     * @function encrypt
     * @param text Text to be encrypted
     * @returns {Progress|Query|*} Returns the same text string encrypted
     * @see https://es.wikipedia.org/wiki/Advanced_Encryption_Standard
     */

    encrypt: function(text){

        var cipher = crypto.createCipher('aes-256-cbc',encryption.getSecret());
        var encrypted = cipher.update(text,'utf-8','hex');
        encrypted += cipher.final('hex');

        return encrypted;
    },

    /**
     *
     * Function that decrypts a text string and then returns it
     *
     * @function decrypt
     * @param text Text to be decrypted
     * @returns {Progress|Query|*} Returns decrypted text string
     * @see https://es.wikipedia.org/wiki/Advanced_Encryption_Standard
     */

    decrypt: function(text){

        var decipher = crypto.createDecipher('aes-256-cbc',encryption.getSecret());
        var decrypted = decipher.update(text,'hex','utf-8');
        decrypted += decipher.final('utf-8');

        return decrypted;

    },

    /**
     *
     * Function that returns a random generated string made with hex characters. Useful when generating random passwords
     *
     * @function getRandomToken
     * @param length Lenght of desired string
     * @returns {String|string|*} Returns a random string
     */

    getRandomToken: function(length){

        return crypto.randomBytes(length).toString('hex');
    },

    /**
     *
     * Function that returns the secret key used in string encryption.
     *
     *
     * @returns {string} Returns the encryption key
     */

    getSecret: function(){

        return 'g00dt1m35Th3r3SG0nN4b3g00dt1m35';
    }
};

module.exports = encryption;