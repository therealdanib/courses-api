/**
 * Mongoose driver configuration
 *
 * @author Julio Bravo <jbravo@clb.unoi.com>
 * @version 0.1.0
 * @module config/mongoose
 *
 */

var env = require('./dev_env');
var mongoose = require('mongoose');

module.exports = function(){

    return mongoose.connect(env.db);
};