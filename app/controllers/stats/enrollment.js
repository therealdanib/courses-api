/**
 *
 * Enrollments statistics controller
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/controllers/stats/enrollment
 *
 */

var Enrollment = require('../../models/enrollment');
var utils = require('../../config/utils');
var mongo = require('mongoose').mongo;

var countByCompanyAggregate = function (companyId) {

    return [

        { $match: { '_companyId': mongo.ObjectId(companyId), 'enrolled':true}},
        { $group: { _id: "$_companyId", count: { $sum: 1 } }}
    ];
};

var EnrollmentController = {

    /**
     *
     * Returns all applications into an array
     *
     * @function countByCompany
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/stats/enrollments/count/byCompany/5700b0fca3280dfd000a
     */

    countByCompany : function(req,res){

        Enrollment.aggregate(countByCompanyAggregate(req.params.companyId), function(err,agg){

            utils.handleSingleResponse(err,agg[0],res);
        });
    }
};

module.exports = EnrollmentController;