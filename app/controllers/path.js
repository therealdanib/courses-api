/**
 *
 * Paths controller
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/controllers/path
 *
 */

var Path = require('../models/path');
var Course = require('../models/course');
var Enroll = require('../models/enrollment');
var mongoose = require('mongoose');
var utils = require('../config/utils');
var async = require('async');

var pathController = {

    /**
     *
     * Gets a complete path by its id and a company
     *
     * @function getOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/path/55DDf86dbf8705054aa2f238/55DDf86dbf8705054aa2f20a
     */

    getOne: function(req,res){

        var _id = req.params.pathId;
        var com = req.params.company;

        Path.findOne({_id:_id, _companyId: com},function(err,path){

            utils.handleSingleResponse(err,path,res);

        }).populate( 'courses.courseId courses.blocksCourses courses.blockedByCourses');
    },

    /**
     *
     * Gets all company paths
     *
     * @function getAll
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/paths/55DDf86dbf8705054aa2f20a
     *
     */

    getAll: function(req,res){

        var com = req.params.company;

        Path.find({_companyId: com},function(err,paths){

            utils.handleBatchResponse(err,paths,res);

        }).populate( 'courses.courseId courses.blocksCourses courses.blockedByCourses');
    },

    /**
     *
     * Creates a path for a company
     *
     * @function createOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * POST / HTTP 1.1
     * /api/v1/path/574df8eb86b676540408cf0a
     * Raw:
     * {
     *  "name": "My First Apithy Path",
     *  "description": "Since we're an educational platform, this will be your first path, which will help you to understand what Apithy is",
     *  "goals": ["To Understand how Apithy can make your life easier", "To have fun"],
     *  "active": true,
     *  "isPublic":true,
     *  "authors": [
     *      "574df8eb86b676540408cf5e",
     *      "574df8eb86b676540408cf5d"
     *  ],
     *  "_companyId": "574df8eb86b676540408cf5e",
     *  "courses":[{
     *      "courseId": "574e0689ba0d450397434eb9",
     *      "order": 1
     *  }]
     *  }
     */

    createOne: function(req,res){

        var p = Object.keys(req.body).length > 0 ? req.body : '';
        if(p === ''){

            res.status(400);
            res.json(utils.getResponseFormat(400,'No parameters have been provided'));
            return;
        }

        p['createDate'] = new Date();
        p['modifyDate']= p.createDate;
        p['openDate']= !p.openDate ? p.createDate : p.openDate;
        p['_companyId'] = req.params.company;
        p['rated'] = {
            'overall': 0,
            'scored': [
                {"rate": 5, "score": 0},
                {"rate": 4, "score": 0},
                {"rate": 3, "score": 0},
                {"rate": 2, "score": 0},
                {"rate": 1, "score": 0}
            ]
        };
        p = new Path(p);

        p.save(function(err){

            utils.handleUpsertResponse(err,p,res);
        });
    },

    /**
     *
     * Updates a path for a company
     *
     * @function updateOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/path/574df8eb86b676540408cf0a/574df8eb86b676540408cf1c
     * Raw:
     * {
     *  "name": "New name",
     *  "isPublic": false
     * }
     */

    updateOne: function(req,res){

        var _id = req.params.pathId;
        var com = req.params.company;
        var p = req.body;
        p.modifyDate = new Date();

        if(p.createDate){

            res.status(400);
            res.json(utils.getResponseFormat(400,'Cannot update original dates'));
            return;
        }

        Path.findOneAndUpdate({_id: _id, _companyId: com}, p, {new: true}, function(err,doc){

            if(err || !doc){

                utils.handleUpsertResponse(err,doc,res);
                return;
            }

            Course.populate(doc,[
                
                { path: 'courses.courseId' }

            ], function(err,result){

                if(err){

                    utils.handleDBError(err,res);
                }
                else{
                    pathController.broadcastEnroll(result,com,res);
                }
            });

        });
    },

    /**
     *
     * Updates internally enrollment objects on path update
     *
     * @function updateOne
     * @param companyId {string} Company id
     * @param path {object} Path update document
     * @param response {object} HTTP Response document
     *
     */

    broadcastEnroll : function(path,companyId,response){

        Enroll.find({'_companyId': companyId, 'paths._pathId':path._id},function(err,enrs){

            if(err){
                
                utils.handleUpsertResponse(err,enrs,response);
            }
            else{

                async.each(enrs, function(enr,cb){

                    var enrPath = enr.paths.find(function(ePath){
                        return ePath._pathId.toString() === path._id.toString();
                    });

                    if(enrPath === undefined){
                        cb("Error updating enrollments");
                        return;
                    }

                    var enrCourses = enrPath.courses;
                    var newCourses = [];
                    path.courses.forEach(function(c){

                        var newCourse = {};
                        newCourse._courseId = c.courseId._id;
                        newCourse.courseOrder = c.order;

                        var found = enrCourses.find(function(enrCourse){
                            return enrCourse._courseId.toString() === c.courseId._id.toString();
                        });

                        if(found){

                            newCourse._id = found._id;
                            newCourse.courseProgress = found.courseProgress;
                            newCourse.courseScore = found.courseScore;
                            newCourse.sessions = found.sessions;
                        }
                        else{

                            newCourse._id = mongoose.Types.ObjectId();
                            newCourse.sessions = [];
                            newCourse.courseProgress = 0;
                            newCourse.courseScore = 0;
                            c.courseId.sessions.forEach(function(session){
                                newCourse.sessions.push({
                                    sessionId: session.sessionId,
                                    sessionOrder: session.order,
                                    sessionProgress: 0,
                                    sessionScore: 0,
                                    _id: mongoose.Types.ObjectId()
                                });
                            });
                        }
                        newCourses.push(newCourse);
                    });

                    enrPath.courses = newCourses;
                    enrPath.pathScore = utils.getAvg(enrPath.courses,'courseScore');
                    enrPath.pathProgress = utils.getAvg(enrPath.courses,'courseProgress');
                    enr.stats.paths.avgScore = utils.getAvg(enr.paths,'pathScore');
                    enr.stats.paths.avgProgress = utils.getAvg(enr.paths,'pathProgress');
                    enrPath.finished = enrPath.pathProgress === 100;

                    enr.save(function (err) {

                        if(err){
                            cb(err);
                        }
                        else cb();
                    });
                }, function(err){

                    utils.handleUpsertResponse(err,path,response);
                });
            }
        });
    },

    /**
     *
     * Deletes a path for a company
     *
     * @function deleteOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /api/v1/path/574df8eb86b676540408cf0a/574df8eb86b676540408cf1c
     *
     */

    deleteOne: function(req,res){

        var _id = req.params.pathId;
        var com = req.params.company;
        Path.findOneAndRemove({_id: _id, _companyId: com}, function(err,path){

            utils.handleUpsertResponse(err,path,res);
        });
    },

    /**
     *
     * Deletes all paths belonging to a company
     *
     * @function deleteByCompany
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /api/v1/path/574df8eb86b676540408cf0a
     */

    deleteByCompany: function(req,res){

        var com = req.params.company;
        Path.remove({_companyId: com}, function(err){

            if(err){

                utils.handleDBError(err,res);
            }
            else{

                res.status(200);
                res.json(utils.getResponseFormat(200));
            }
        });
    }
};

module.exports = pathController;