/**
 *
 * Applications controller
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/controllers/application
 *
 */

var Application = require('../models/application');
var utils = require('../config/utils');
var security = require('../config/security');
var encryption = require('../config/encryption');
var uuid = require('node-uuid');

var appsController = {

    /**
     *
     * Returns all applications into an array
     *
     * @function getAll
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/applications
     */

    getAll: function(req,res) {

        Application.find(function(err,applications){

            if(err){

                utils.handleDBError(err, res);

            }else{

                utils.handleBatchResponse(err,applications,res);
            }
        });
    },

    /**
     *
     * Registers a new application
     *
     * @function create
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * POST / HTTP 1.1
     * /v1/application
     * Raw: {
        "applicationName": "Demo app",
        "active": true
     * }
     */

    createOne: function(req,res){

        var application = new Application(req.body);

        if(!appsController.validateUpsertRequest(req)){

            res.status(400);
            res.json(utils.getResponseFormat(400,'Some application fields are not accepted',null));
            return;
        }

        var passwd = encryption.getRandomToken(13);
        application.created = new Date();
        application.modifyDate = application.created;
        application.password = encryption.encrypt(passwd);
        application.publicId = uuid.v4();

        application.save(function (err){

            if(err) {

                utils.handleDBError(err, res);

            } else {

                res.status(200);
                res.json(utils.getResponseFormat(200,null,{"_id":application._id, "password": passwd}));
            }
        });
    },

    /**
     *
     * Returns an application through its id given in the request url
     *
     * @function getById
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /v1/application/55DDf86dbf8705054aa2f238
     */

    getById: function(req,res){

        var _id = req.params.appId;

        Application.findOne({_id: _id}, function (err,application){

            if(err){

                utils.handleDBError(err, res);
            }
            else{

                utils.handleSingleResponse(err,application,res);
            }
        });
    },

    /**
     * Updates a registered application
     * @function update
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /v1/application/55DDf86dbf8705054aa2f238
     * Raw: {
        "applicationName": "Demo app new name",
        "active": true
     * }
     *
     */

    update: function(req,res){

        if(!appsController.validateUpsertRequest(req)){

            res.status(400);
            res.json(utils.getResponseFormat(400,"Some application fields are not updatable",null));
            return;
        }

        var app = req.body;
        app.modifyDate = new Date();

        Application.findByIdAndUpdate(req.params.appId, app, {new: true}, function(err,application) {

            if(err) {

                utils.handleDBError(err, res);
            }
            else{

                res.status(200);
                res.json(utils.getResponseFormat(200,null,{
                    "publicId": application.publicId,
                    "newCredentials": security.generateToken(application,30)
                }));
            }
        });
    },

    /**
     *
     * Deletes an application through an id given
     *
     * @function delete
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /v1/application/55DDf86dbf8705054aa2f238
     */

    delete: function(req,res){

        Application.findByIdAndRemove(req.params.appId,function(err,app){

            if(err){

                utils.handleDBError(err, res);
            }
            else {

                utils.handleUpsertResponse(err,app,res);
            }
        });
    },

    /**
     *
     * Function that validates the request object before it's inserted or updated
     *
     * @private
     * @function validateUpsertRequest
     * @param request {object} Request object
     * @returns {boolean} Returns whether request is valid or not
     *
     */

    validateUpsertRequest : function(request){

        return (Object.keys(request.body).length !== 0 && !request.body.created && !request.body._id);
    }
};
module.exports = appsController;