/**
 *
 * Courses controller
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/controllers/course
 *
 */

var Course = require('../models/course');
var Enroll = require('../models/enrollment');
var utils = require('../config/utils');
var async = require('async');
var uuid = require('node-uuid');
var mongoose = require('mongoose');

var courseController = {

    /**
     *
     * Gets a course by its id and company id
     *
     * @function getOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/course/55DDf86dbf8705054aa2f238/55DDf86dbf8705054aa2f238
     */

    getOne: function(req,res){

        var _id = req.params.courseId;
        var company = req.params.companyId;

        Course.findOne({_id:_id, _companyId: company},function(err,course){

            utils.handleSingleResponse(err,course,res);

        }).populate('sessions.sessionType');
    },

    /**
     *
     * Gets a list of courses by company
     *
     * @function getByCompany
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/courses/55DDf86dbf8705054aa2f238
     */

    getByCompany: function(req,res){

        var companyId = req.params.companyId;
        Course.find({_companyId: companyId}, function(err,courses){

            utils.handleBatchResponse(err,courses,res);

        }).populate('sessions.sessionType');
    },

    /**
     * Creates a course giving a company id to be associated to
     *
     * @function createOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * POST / HTTP 1.1
     * /api/v1/course/55DDf86dbf8705054aa2f238
     * Raw:
     * {
     *  "name": "Demo course part 2",
     *  "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
     *  "isPublic": true,
     *  "validated": true,
     *  "authors": ["574df8eb86b676540408cf5e","574df8eb86b676540408cf5d"]
     * }
     */

    createOne: function(req,res){

        var c  = Object.keys(req.body).length > 0 ? req.body : '';

        if(c === ''){

            res.status(400);
            res.json(utils.getResponseFormat(400,'No parameters have been provided'));
            return;
        }

        c['createDate']= new Date();
        c['modifyDate']= c.createDate;
        c['openDate'] = !c.openDate ? c.createDate : c.openDate;
        c['_companyId'] = req.params.companyId;
        c['rated'] = {
            'overall': 0,
            'scored': [
                {"rate": 5, "score": 0},
                {"rate": 4, "score": 0},
                {"rate": 3, "score": 0},
                {"rate": 2, "score": 0},
                {"rate": 1, "score": 0}
            ]
        };
        c = new Course(c);
        c.save(function(err){

            utils.handleUpsertResponse(err,c,res);
        });
    },

    /**
     *
     * Updates a course by its id and company
     *
     * @function updateOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/course/55DDf86dbf8705054aa2f238/55DDf86dbf8705054aa2f200a
     * Raw:
     * {
     *  "name": "Demo course part 2",
     *  "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
     *  "sessions":[
     *      {
     *          "sessionId": "dasndjnsadnjsa-21921921-2192912",
     *          "order": 2,
     *          "edited": true
     *      }
     *  ]
     * }
     */

    updateOne: function(req,res){

        var _id = req.params.courseId;
        var comId = req.params.companyId;

        var c = req.body;
        c.modifyDate = new Date();

        c.sessions.forEach(function (session) {

            session.sessionId = session.sessionId === undefined ? uuid.v4() : session.sessionId;
        });

        Course.findOneAndUpdate({_id: _id, _companyId: comId}, c, {new: true}, function(err,doc){

            if(err || !doc){
                
                utils.handleUpsertResponse(err,doc,res);
                return;
            }
            courseController.broadcastEnroll(_id,c,comId,res);
        });
    },


    /**
     *
     * Updates internally enrollment objects on course update
     *
     * @function updateOne
     * @param id {string} Course id
     * @param companyId {string} Company id
     * @param doc {object} Course update document
     * @param response {object} HTTP Response document
     *
     */

    broadcastEnroll : function(id,doc,companyId,response){

        var q = { '_companyId':companyId ,$or: [ { 'courses._courseId': id }, { 'paths.courses._courseId': id } ]};

        Enroll.find(q,function(err,enrs){

            if(err){

                utils.handleDBError(err,response);
            }
            else{

                async.each(enrs, function(enr,cb){

                    var enrCourses = [];
                    var enrCourse = enr.courses.find(function(c){
                        return c._courseId.toString() === id.toString();
                    });
                    if (enrCourse !== undefined){
                        enrCourses.push(enrCourse);
                    }

                    var enrPaths = enr.paths.filter(function(p){

                        var found = p.courses.find(function (c) {
                            return c._courseId.toString() === id.toString();
                        });
                        if(found !== undefined){
                            enrCourses.push(found);
                            return p;
                        }
                    });

                    enrCourses.forEach(function(enrCourse){
                        var newSessions = [];
                        doc.sessions.forEach(function(s){

                            var foundSession = enrCourse.sessions.find(function (enrSession) {
                                return enrSession.sessionId === s.sessionId;
                            });
                            newSessions.push({
                                _id: foundSession ? foundSession._id : mongoose.Types.ObjectId(),
                                sessionOrder: s.order,
                                sessionId: foundSession ? foundSession.sessionId : s.sessionId,
                                sessionProgress : s.edited || foundSession === undefined ? 0 : foundSession.sessionProgress,
                                sessionScore : s.edited || foundSession === undefined ? 0 : foundSession.sessionScore
                            });
                        });
                        enrCourse.sessions = newSessions;
                        enrCourse.courseScore = utils.getAvg(enrCourse.sessions,'sessionScore');
                        enrCourse.courseProgress = utils.getAvg(enrCourse.sessions,'sessionProgress');
                        enrCourse.finished = enrCourse.courseProgress === 100;
                    });

                    enrPaths.forEach(function(path){
                        path.pathProgress = utils.getAvg(path.courses,'courseProgress');
                        path.pathScore = utils.getAvg(path.courses,'courseScore');
                        path.finished = path.pathProgress === 100;
                    });

                    enr.stats.paths.avgScore = utils.getAvg(enr.paths,'pathScore');
                    enr.stats.paths.avgProgress = utils.getAvg(enr.paths,'pathProgress');
                    enr.stats.courses.avgScore = utils.getAvg(enr.courses,'courseScore');
                    enr.stats.courses.avgProgress = utils.getAvg(enr.courses,'courseProgress');

                    enr.save(function (err) {

                        if(err){
                            cb(err);
                        }
                        else cb();
                    });

                }, function(err){

                    doc._id = id;
                    utils.handleUpsertResponse(err,doc,response);
                });
            }
        });
    },

    /**
     *
     * Adds one or more sessions to a course given its id and company id
     *
     * @function addSession
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/course/addSessions/55DDf86dbf8705054aa2f238/55DDf86dbf8705054aa2f200a
     * Raw:
     * [
     *  {
     *      "name": "Demo course part 2",
     *      "s3ContentRef": "https://s3.amazonaws.com/demo/democontent.html",
     *      "sessionType": "55DDf86dbf8705054aa2f238",
     *      "order": 1
     *  }
     * ]
     *
     */

    addSessions: function(req,res){

        var _id = req.params.courseId;
        var _company = req.params.companyId;
        var sessions = req.body;

        if(!sessions || !sessions.length){

            res.status(400);
            res.json(utils.getResponseFormat(400,'Missing parameters'));
            return;
        }

        var updateObject = {

            $push: { 'sessions': { $each: sessions } },
            'modifyDate': new Date()
        };

        Course.findOneAndUpdate({'_id':_id, '_companyId': _company}, updateObject, {new: true}, function(err,enr){

            utils.handleUpsertResponse(err,enr,res);
        });
    },

    /**
     *
     * Deletes a course by its id and company
     *
     * @function deleteOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /api/v1/course/55DDf86dbf8705054aa2f238/55DDf86dbf8705054aa2f200a
     *
     */

    deleteOne: function(req,res){

        var _id = req.params.courseId;
        var comId = req.params.companyId;

        Course.findOneAndRemove({_id: _id, _companyId: comId}, function(err,course){

            utils.handleUpsertResponse(err,course,res);
        });
    },

    /**
     *
     * Deletes all courses belonging to a company
     *
     * @function deleteOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /api/v1/courses/55DDf86dbf8705054aa2f238
     *
     */

    deleteByCompany: function(req,res){

        var companyId = req.params.companyId;
        Course.remove({_companyId: companyId}, function(err){

            if(err){

                utils.handleDBError(err,res);
            }
            else{

                res.status(200);
                res.json(utils.getResponseFormat(200));
            }
        });
    }
};

module.exports = courseController;