/**
 *
 * Enrollments controller
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/controllers/enrollment
 *
 */

var Enrollment = require('../models/enrollment');
var Course = require('../models/course');
var utils = require('../config/utils');

var EnrollmentController = {

    /**
     *
     * Gets an enrollment by its id
     *
     * @function getOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/enrollment/55DDf86dbf8705054aa2f238
     */

    getOne: function (req,res){

        var _id = req.params._id;

        Enrollment.findOne({_id:_id},function(err,enr){

            if(err){

                utils.handleDBError(err,res);
                return;
            }

            Course.populate(enr,[

                { path: 'paths._pathId.courses.courseId' },
                { path: 'paths.courses._courseId' }

            ], function(err,result){

                utils.handleSingleResponse(err,result,res);
            });

        }).populate( 'paths._pathId courses._courseId');

    },

    /**
     * Gets all enrollments by a given company
     *
     * @function getByCompany
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/enrollments/55DDf86dbf8705054aa2f238
     */

    getByCompany: function(req,res){

        var _companyId = req.params._companyId;

        Enrollment.find({_companyId: _companyId},function(err,enrs){

            if(err){

                utils.handleDBError(err,res);
                return;
            }

            Course.populate(enrs,[
                { path: 'paths._pathId.courses.courseId' },
                { path: 'paths.courses._courseId' }

            ], function(err,result){

                utils.handleBatchResponse(err,result,res);
            });

        }).populate('paths._pathId courses._courseId');

    },

    /**
     *
     * Gets the user's enrollment information associated to a given company.
     * Returns a single enrollment JSON object
     *
     * @function getByUser
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/enrollment/byUser/55DDf86dbf8705054aa2f231/55DDf86dbf8705054aa2f238
     */

    getByUser: function(req,res){

        var _companyId = req.params._companyId;
        var _userId = req.params._userId;

        Enrollment.findOne({_userId:_userId, _companyId:_companyId},function(err,enr){
            if(err){

                utils.handleDBError(err,res);
                return;
            }

            Course.populate(enr,[
                { path: 'paths._pathId.courses.courseId'},
                { path: 'paths.courses._courseId' }

            ], function(err,result){

                utils.handleSingleResponse(err,result,res);
            });

        }).populate( 'paths._pathId courses._courseId');
    },

    /**
     *
     * Gets all enrollments associated to a given course in a company
     *
     * @function getByCourse
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/enrollments/byCourse/55DDf86dbf8705054aa2f238/55DDf86dbf8705054aa2f23e
     */

    getByCourse: function(req,res){

        var _courseId = req.params.courseId;
        var _companyId = req.params.companyId;
        var query = { '_companyId':_companyId ,$or: [ { 'courses._courseId': _courseId }, { 'paths.courses._courseId': _courseId } ]};

        Enrollment.find(query,function(err,enrs){

            if(err){

                utils.handleDBError(err,res);
                return;
            }

            Course.populate(enrs,[
                { path: 'paths._pathId.courses.courseId' },
                { path: 'paths.courses._courseId' }

            ], function(err,result){

                utils.handleBatchResponse(err,result,res);
            });

        }).populate( 'paths._pathId courses._courseId');
    },

    /**
     *
     * Gets all enrollments associated to a given path in a company
     *
     * @function getByPath
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/enrollments/byPath/55DDf86dbf8705054aa2f23e/55DDf86dbf8705054aa2f238
     */

    getByPath: function(req,res){

        var _pathId = req.params.pathId;
        var _companyId = req.params.companyId;

        Enrollment.find({'_companyId': _companyId, 'paths._pathId':_pathId},function(err,enrs){

            if(err){

                utils.handleDBError(err,res);
                return;
            }

            Course.populate(enrs,[
                { path: 'paths._pathId.courses.courseId' },
                { path: 'paths.courses._courseId' }

            ], function(err,result){

                utils.handleBatchResponse(err,result,res);
            });

        }).populate( 'paths._pathId courses._courseId');
    },

    /**
     *
     * Creates an enrollment document
     *
     * @function createOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * POST / HTTP 1.1
     * /api/v1/enrollment
     * Raw:
     *  {
     *      "_userId": "574df8eb86b676540408cf5e",
     *      "_companyId": "574df8eb86b676540408cf5a",
     *      "courses": [{
     *          "_courseId": "574df8eb86b676540408cf5e"
     *      }],
     *      "paths": [{
     *          "_pathId": "574df8eb86b676540408cf4f"
     *      }]
     *  }
     */

    createOne: function (req,res){

        var e = Object.keys(req.body).length > 0 ? req.body : '';
        if(e === ''){

            res.status(400);
            res.json(utils.getResponseFormat(400,'No parameters have been provided'));
            return;
        }

        e['dateEnrolled'] = new Date();
        e['lastModification']= e.dateEnrolled;
        e = new Enrollment(e);

        e.save(function(err){

            utils.handleUpsertResponse(err,e,res);
        });
    },

    /**
     * Creates bulk enrollments
     *
     * @function createBulk
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * POST / HTTP 1.1
     * /api/v1/enrollments
     * Raw:
     * [
     *  {
     *      "_userId": "574df8eb86b676540408cf5e",
     *      "_companyId": "574df8eb86b676540408cf5a",
     *      "courses": [{
     *          "_courseId": "574df8eb86b676540408cf5e"
     *      }],
     *      "paths": [{
     *          "_pathId": "574df8eb86b676540408cf4f"
     *      }]
     *  },
     *  {
     *      "_userId": "574df8eb86b676540408cf5c",
     *      "_companyId": "574df8eb86b676540408cf5a",
     *      "courses": [{
     *          "_courseId": "574df8eb86b676540408cf5e"
     *      }],
     *      "paths": [{
     *          "_pathId": "574df8eb86b676540408cf4f"
     *      }]
     *  }
     * ]
     */

    createBulk: function(req,res){

        var enrs = req.body;

        enrs.forEach(function(e){

            e['dateEnrolled'] = new Date();
            e['lastModification']= e.dateEnrolled;
        });

        if(enrs.length === 0){

            res.status(400);
            res.json(utils.getResponseFormat(400,'No parameters have been provided'));
            return;
        }

        Enrollment.insertMany(enrs,function(err,docs){

            utils.handleBulkOpResponse(err,docs,res);
        });
    },

    /**
     *
     * Updates an enrollment document by its id, or user id with company id
     *
     * @function updateOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/enrollment/574df8eb86b676540408c00a
     * /api/v1/enrollment/574df8eb86b676540408c00a/574df8eb86b676540408c00b
     * Raw:
     *  {
     *      "courses": [{
     *          "_courseId": "574df8eb86b676540408cf5e",
     *          "finished": true
     *      }],
     *      "paths": [
     *          {
     *              "_pathId": "574df8eb86b676540408cf4f"
     *          },
     *          {
     *              "_pathId": "574df8eb86b676540408cf5a"
     *          }
     *      ]
     *  }
     */

    updateOne: function(req,res){

        var e = req.body;
        e.lastModification = new Date();

        if(e.dateEnrolled || e._userId){

            res.status(400);
            res.json(utils.getResponseFormat(400,'Some parameters you provided cannot be updated'));
            return;
        }

        Enrollment.findOneAndUpdate(req.params, e, {new: true}, function(err,enr){

            utils.handleUpsertResponse(err,enr,res);
        });
    },

    /**
     *
     * Updates an enrollment's metrics passing its id, and in the body an internal course id, session id, optionally a path id and
     * their corresponding progress and/or score values.
     *
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/enrollment/updateMetrics/574df8eb86b676540408c00a
     * Raw:
     * {
     *    "course":{
     *        "_id": "57aa244a2683fde75b5b3f02",
     *        "progress": 100,
     *        "score": 100
     *    },
     *    "session":{
     *        "_id": "57aa244a2683fde75b5b3f06",
     *        "progress": 100,
     *        "score": 100
     *    }
     * }
     */

    updateMetrics: function(req,res){

        var _id = req.params.enrId;
        var metrics = req.body;

        if(Object.keys(metrics).length === 0 || !EnrollmentController.areMetricsValid(metrics)){

            res.status(400);
            res.json(utils.getResponseFormat(400,'No parameters have been provided'));
            return;
        }

        Enrollment.findById(_id, function(err,enr){

            if(err || !enr){
                utils.handleUpsertResponse(err,enr,res);
            }
            else{
                EnrollmentController.internalMetrics(enr,metrics,res);
            }
        });
    },
    
    /**
     *
     * Function to internally update document metrics
     * 
     * @function internalMetrics
     * @private
     * @param enr {object} Enrollment object found previously
     * @param metrics {object} JSON Object that contains metrics to be updated
     * @param res {object} Response object
     *
     */

    internalMetrics : function(enr,metrics,res){

        var dateFinished = new Date();
        var path = metrics.path ? enr.paths.id(metrics.path._id) : undefined;
        var courses = path ? path.courses : undefined;
        var course = path ? enr.paths.id(metrics.path._id).courses.id(metrics.course._id) : enr.courses.id(metrics.course._id);
        var session = course.sessions.id(metrics.session._id);

        session.sessionProgress = metrics.session.progress;
        session.sessionScore = metrics.session.score;
        session.value = metrics.session.value;
        
        course.courseScore = course.courseScore + ((session.value*session.sessionScore)/100);
        course.courseProgress = course.courseProgress + ((session.value*session.sessionProgress)/100);
        course.lastVisited = dateFinished;
        
        if(course.courseProgress === 100){
            
            course.finished = true;
            course.finishedDate = dateFinished;
        }
        else{

            course.finished = false;
        }
        
        if(path && courses){

            var pathScore = utils.getAvg(courses,'courseScore');
            var pathProgress = utils.getAvg(courses,'courseProgress');

            path.pathProgress = pathProgress;
            path.pathScore = pathScore;
            path.lastVisited = dateFinished;

            if(pathProgress === 100){

                path.finished = true;
                path.finishedDate = dateFinished;

            }else{

                path.finished = false;
            }
        }

        enr.stats.paths = {
            
            'avgScore': utils.getAvg(enr.paths,'pathScore'),
            'avgProgress': utils.getAvg(enr.paths,'pathProgress')
            
        };

        enr.stats.courses = {

            'avgScore': utils.getAvg(enr.courses,'courseScore'),
            'avgProgress': utils.getAvg(enr.courses,'courseProgress')
        };

        enr.save(function(err,saved){

            utils.handleUpsertResponse(err,saved,res);
        });
    },

    /**
     *
     * Validates that JSON body contains required keys in order to update metrics
     *
     * @function areMetricsValid
     * @private
     * @param metrics {object} JSON body
     * @returns {boolean} Boolean value that indicates wheter body is valid or not
     */

    areMetricsValid : function(metrics){

        var keys = [
            '_id',
            'progress',
            'score',
            'value'
        ].sort().toString();
        
        if(!metrics.course || !metrics.session || (metrics.path && !metrics.path._id)){
            return false;
        }

        return metrics.course._id && Object.keys(metrics.session).sort().toString() === keys;
    },

    /**
     *
     * Adds a path or course to an enrollment given its id
     *
     * @function addItem
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/enrollment/addPath/574df8eb86b676540408ca01
     * /api/v1/enrollment/addCourse/574df8eb86b676540408ca01
     * {
     *      "_pathId": "574df8eb86b676540408cf5e",
     *      "courses": [
     *          {
     *              "_courseId": "574df8eb86b676540408cf5e",
     *              "courseScore": 0,
     *              "courseOrder": 0
     *          }
     *      ],
     *      "finished": false
     * }
     *
     */

    addElement: function(req,res){

        var _id = req.params.enrollment;
        var item = req.body;
        var collection;

        if (item === undefined || Object.keys(item).length === 0){

            res.status(400);
            res.json(utils.getResponseFormat(400,'Missing parameters'));
            return;
        }

        switch (req.params.elemType){

            case 'Path':
                collection = 'paths';
                break;

            case 'Course':
                collection = 'courses';
                break;

            default:

                res.status(400);
                res.json(utils.getResponseFormat(400,'Unknown item type'));
                return;
        }

        var pushObj =JSON.parse('{"' + collection + '":' + JSON.stringify(item)+ '}');

        var updateObject = {

            $push: pushObj,
            'lastModification': new Date(),
            'enrolled': true
        };

        Enrollment.findByIdAndUpdate(_id, updateObject, {new: true}, function(err,enr){

            if(err || !enr){

                utils.handleUpsertResponse(err,enr,res);
                return;
            }

            if(req.params.elemType === 'Course'){

                enr.stats.courses = {

                    'avgProgress': utils.getAvg(enr.courses,'courseProgress'),
                    'avgScore': utils.getAvg(enr.courses,'courseScore')
                };

            }else if(req.params.elemType === 'Path'){

                enr.stats.paths = {

                    'avgProgress': utils.getAvg(enr.paths,'pathProgress'),
                    'avgScore': utils.getAvg(enr.paths,'pathScore')
                };
            }

            enr.save(function(err,saved){

                utils.handleUpsertResponse(err,saved,res);
            });
        });
    },

    /**
     *
     * Deletes an enrollment document by its id
     *
     * @function deleteOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /api/v1/enrollment/574df8eb86b676540408c00a
     *
     */

    deleteOne: function(req,res){

        var _id = req.params._id;
        Enrollment.findByIdAndRemove(_id,function(err,enr){

            if(err){

                utils.handleDBError(err,res);
            }
            else{

                utils.handleUpsertResponse(err,enr,res);
            }
        });
    },

    /**
     * Deletes all enrollments by a given company
     *
     * @function deleteAll
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /api/v1/enrollments/574df8eb86b676540408c00a
     */

    deleteAll: function(req,res){

        var _companyId = req.params._companyId;
        Enrollment.remove({_companyId: _companyId}, function(err,enrs){

            utils.handleBulkOpResponse(err,enrs,res);
        });
    }
};

module.exports = EnrollmentController;