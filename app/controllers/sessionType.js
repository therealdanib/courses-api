/**
 *
 * Session types controller
 *
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/controllers/sessionType
 *
 */

var SessionType = require('../models/sessionType');
var utils = require('../config/utils');

var sessiontypeController = {


    /**
     *
     * Gets all existing sessions types
     *
     * @function getAll
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/sessionTypes
     *
     */

    getAll: function(req,res){

        SessionType.find(function(err, stypes){

            utils.handleBatchResponse(err,stypes,res);
        });
    },

    /**
     *
     * Gets one session type by its id
     *
     * @function getAll
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * GET / HTTP 1.1
     * /api/v1/sessionType/55DDf86dbf8705054aa2f238
     */

    getOne: function(req,res){

        var _id = req.params.sessionTypeId;
        SessionType.findOne({_id: _id},function(err,stype){

            utils.handleSingleResponse(err,stype,res);
        });
    },

    /**
     *
     * Creates one session type
     *
     * @function createOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * POST / HTTP 1.1
     * /api/v1/sessionType
     * Raw:
     * {
     *  "name": "HTML",
     *  "active": true
     * }
     */

    createOne: function(req,res){

        var st = new SessionType(req.body);
        st.save(function(err){

            utils.handleUpsertResponse(err,st,res);
        });
    },

    /**
     *
     * Updates one session type through its id
     *
     * @function updateOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * PUT / HTTP 1.1
     * /api/v1/sessionType/55DDf86dbf8705054aa2f238
     * Raw:
     * {
     *  "name": "HTML Improved"
     * }
     */

    updateOne: function(req,res){

        var _id = req.params.sessionTypeId;
        var st = req.body;

        SessionType.findByIdAndUpdate(_id,st,{new:true}, function(err,stype){

            utils.handleUpsertResponse(err,stype,res);
        });
    },

    /**
     * Deletes one session type through its id
     *
     * @function deleteOne
     * @param req {object} Request object
     * @param res {object} Response object
     * @example
     * DELETE / HTTP 1.1
     * /api/v1/sessionType/55DDf86dbf8705054aa2f238
     *
     */

    deleteOne: function(req,res){

        var _id = req.params.sessionTypeId;
        SessionType.findByIdAndRemove(_id,function(err,stype){

            utils.handleUpsertResponse(err,stype,res);
        });
    }
};

module.exports = sessiontypeController;