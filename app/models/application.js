/**
 * Applications model
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/models/application
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idValidator = require('mongoose-id-validator');

var applicationSchema = new Schema({
    publicId: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false,
        default: ''
    },
    logoApp: {
        type: String,
        required: false,
        default: ''
    },
    typeApp: {
        type: String,
        required: true,
        default: 'Web',
        enum: ['Web', 'Mobile', 'Api']
    },
    urlApp: {
        type: String,
        required: false,
        default: 'http://'
    },
    active:{
        type: Boolean,
        default: true,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    modifyDate:{
        type: Date,
        required: true
    }
},{
    versionKey: false
});

applicationSchema.index({publicId:1},{unique:true});
applicationSchema.plugin(idValidator);

var applicationsDB = mongoose.model('Applications', applicationSchema);
module.exports = applicationsDB;