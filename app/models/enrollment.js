/**
 * Enrollments model
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/models/enrollment
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idValidator = require('mongoose-id-validator');
var Course = require('./course');
var Path = require('./path');
var uuid = require('node-uuid');

var EnrollmentModel = new Schema({

    _userId : {

        type: Schema.Types.ObjectId,
        required: true
    },
    _companyId:{

        type: Schema.Types.ObjectId,
        required: true
    },
    enrolled: {

        type: Boolean,
        default: false,
        required: true
    },
    dateEnrolled:{

        type: Date,
        required: true
    },
    lastModification:{

        type: Date,
        required: true
    },
    stats:{

        paths:{

            avgScore:{

                type: Number,
                default: 0.0,
                min: 0, max: 100,
                required: true
            },
            avgProgress:{

                type: Number,
                default: 0.0,
                min: 0, max: 100,
                required: true
            }
        },
        courses:{

            avgScore:{

                type: Number,
                default: 0.0,
                min: 0, max: 100,
                required: true
            },
            avgProgress:{

                type: Number,
                default: 0.0,
                min: 0, max: 100,
                required: true
            }
        }
    },
    paths: [{

        _pathId: {

            type: Schema.Types.ObjectId,
            ref: 'Path'
        },
        pathScore:{

            type: Number,
            default: 0.0,
            min: 0, max: 100
        },
        pathProgress:{

            type: Number,
            default: 0.0,
            min: 0, max: 100
        },

        rated: {

            type: Number,
            default: 0,
            min: 0
        },
        finished: {

            type: Boolean,
            required: true,
            default: false
        },
        finishedDate:{
            type: Date
        },
        lastVisited: {
            
            type: Date 
        },
        courses:[{

            _courseId: {

                type: Schema.Types.ObjectId,
                ref: 'Course',
                required: true
            },
            courseOrder:{

                type: Number,
                required: true
            },
            courseScore: {

                type: Number,
                default: 0.0,
                min: 0, max: 100
            },
            courseProgress: {

                type: Number,
                default: 0.0,
                min: 0, max: 100
            },
            rated: {

                type: Number,
                default: 0,
                min: 0
            },
            sessions: [{

                sessionId: {

                    type: String,
                    default: uuid.v4
                },
                sessionOrder: {

                    type: Number
                },
                sessionScore: {

                    type: Number,
                    default: 0.0,
                    min: 0, max: 100
                },
                sessionProgress: {

                    type: Number,
                    default: 0.0,
                    min: 0, max: 100
                }
            }],
            finished: {

                type: Boolean,
                required: true,
                default: false
            },
            finishedDate:{
                type: Date
            },
            lastVisited: {

                type: Date
            }
        }]
    }],

    courses: [{

        _courseId: {

            type: Schema.Types.ObjectId,
            ref: 'Course',
            required: true
        },
        courseScore: {

            type: Number,
            default: 0.0,
            min: 0, max: 100
        },
        courseProgress: {

            type: Number,
            default: 0.0,
            min: 0, max: 100
        },
        rated: {

            type: Number,
            default: 0,
            min: 0
        },
        sessions: [{

            sessionId: {

                type: String,
                default: uuid.v4
            },
            sessionOrder: {

                type: Number
            },
            sessionScore: {

                type: Number,
                default: 0.0,
                min: 0, max: 100
            },
            sessionProgress: {

                type: Number,
                default: 0.0,
                min: 0, max: 100
            }
        }],
        finished: {

            type: Boolean,
            required: true,
            default: false
        },
        finishedDate:{
            type: Date
        },
        lastVisited: {
            
            type: Date 
        }
    }]
},{
    versionKey: false
});

EnrollmentModel.index({ _userId: 1, _companyId: 1 }, { unique: true });
EnrollmentModel.index({ 'courses._courseId': 1});
EnrollmentModel.index({ 'paths._pathId': 1});
EnrollmentModel.index({ 'sessions._sessionId': 1});
EnrollmentModel.index({ _companyId: 1, enrolled: 1 });
EnrollmentModel.plugin(idValidator);

module.exports = mongoose.model('Enrollment',EnrollmentModel);