/**
 * Session types model
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/models/sessionTypes
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SessionTypeModel = new Schema({

    name: {
        type: String,
        required: true
    },
    active: {

        type: Boolean,
        required: true,
        default: true
    }
},{
    versionKey: false
});

SessionTypeModel.index({ active: 1 });
SessionTypeModel.index({ name: 1 }, { unique: true });
module.exports = mongoose.model('SessionType',SessionTypeModel);