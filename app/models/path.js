/**
 * Paths model
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/models/path
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idValidator = require('mongoose-id-validator');
var Course = require('./course');

var PathModel = new Schema({

    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    preview: {
        url: {
            type: String
        },
        format: {
            type: String,
            enum: ['image', 'video', 'embed']
        }
    },
    goals: [{
        type: String,
        required: true
    }],
    openDate: {
        type: Date,
        required: true
    },
    closeDate: {
        type: Date
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    isPublic: {
        type: Boolean,
        required: true,
        default: false
    },
    clonedFrom: {
        type: Schema.Types.ObjectId,
        ref: 'Path'
    },
    validated:{
        type: Boolean,
        required: true,
        default: false
    },
    authors: [{
        type: Schema.Types.ObjectId,
        required: true
    }],
    createDate: {
        type: Date,
        required: true
    },
    modifyDate: {
        type: Date,
        required: true
    },
    rated:{
        overall: {
            type: Number,
            required: true,
            default: 0
        },
        scored: [
            {
                "rate": {
                    type: Number,
                    required: true,
                    default: 0,
                    min: 1, max: 5
                },
                "score": {
                    type: Number,
                    required: true,
                    default: 0
                }
            }
        ]
    },
    _companyId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    courses: [{

        courseId: {
            type: Schema.Types.ObjectId,
            ref: 'Course',
            required: true
        },
        order: {

            type: Number,
            required: true
        },
        blocksCourses: [{

            type: Schema.Types.ObjectId,
            ref: 'Course'
        }],
        blockedByCourses:[{

            type: Schema.Types.ObjectId,
            ref: 'Course'
        }]
    }]
},{
    versionKey: false
});

PathModel.index({name: 1, _companyId:1}, {unique: true});
PathModel.index({_companyId: 1});
PathModel.index({_id: 1, _companyId: 1});
PathModel.plugin(idValidator);

module.exports = mongoose.model('Path',PathModel);