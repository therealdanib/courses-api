/**
 * Courses model
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 1.0.0
 * @module app/models/course
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idValidator = require('mongoose-id-validator');
var SessionType = require('./sessionType');
var uuid = require('node-uuid');

var CourseModel = new Schema({

    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    preview: {
        url: {
            type: String
        },
        format: {
            type: String,
            enum: ['image', 'video', 'embed']
        }
    },
    isDemo: {
        type: Boolean,
        default: false,
        required: true
    },
    clonedFrom: {
        type: Schema.Types.ObjectId,
        ref: 'Course'
    },
    goals: [{
        type: String,
        required: true
    }],
    openDate: {
        type: Date,
        required: true
    },
    closeDate: {
        type: Date
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    isPublic: {
        type: Boolean,
        required: true,
        default: false
    },
    validated: {
        type: Boolean,
        required: true,
        default: true
    },
    validationComments: [{
        type: String
    }],
    authors: [{
        type: Schema.Types.ObjectId,
        required: true,
        unique: true
    }],
    createDate: {
        type: Date,
        required: true
    },
    modifyDate: {
        type: Date,
        required: true
    },
    s3RelatedContent: {
        type: String
    },
    rated:{
        overall: {
            type: Number,
            required: true,
            default: 0
        },
        scored: [
            {
                "rate": {
                    type: Number,
                    required: true,
                    default: 0,
                    min: 1, max: 5
                },
                "score": {
                    type: Number,
                    required: true,
                    default: 0
                }
            }
        ]
    },
    _companyId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    sessions: [{

        sessionId:{
            type: String,
            default: uuid.v4
        },
        name: {
            type: String,
            required: true
        },
        s3RelatedContentRef: {

            type: String
        },
        sessionType: {

            type: Schema.Types.ObjectId,
            ref: 'SessionType',
            required: true
        },
        s3ContentRef: {

            type: String
        },
        required: {

            type: Boolean,
            required: true,
            default: true
        },
        order:{

            type: Number,
            required: true
        },
        value:{

            type: Number
        },
        minTime:{

            type: Number,
            default: 0
        },
        minScore: {

            type: Number,
            default: 0
        },
        blockType:{

            type: String,
            enum: ["none","score","progress","mixed"],
            default: "none"
        },
        active:{

            type: Boolean,
            required: true,
            default: true
        },
        blocksSessions: [{

            type: Number
        }],
        blockedBySessions: [{

            type: Number
        }],
        topics:[{

            name: {
                type: String,
                required: true
            },
            order:{
                type: Number,
                required: true,
                min: 1
            },
            description: {
                type: String,
                required: true
            },
            type:{
                type:String,
                enum:['image','embed']
            },
            url: {
                type: String,
                required: true
            }
        }]
    }]
},{
    versionKey: false
});

CourseModel.index({name: 1, _companyId:1}, {unique:true});
CourseModel.index({_companyId: 1});
CourseModel.index({_id: 1, _companyId: 1});
CourseModel.plugin(idValidator);

module.exports = mongoose.model('Course',CourseModel);