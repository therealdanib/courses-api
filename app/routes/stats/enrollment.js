/**
 * Enrollments stats router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/routes/stats/enrollment
 */

var enrStats = require('../../controllers/stats/enrollment');
var env = require('../../config/dev_env');
var utils = require('../../config/utils');
var sec = require('../../config/security');

var enrStatsRoutes= function(app){

    app.route(env.versionUrl+'stats/enrollments/count/byCompany/:companyId')
        .get([sec.grantAccess],enrStats.countByCompany)
        .all(utils.handleNotAllowed);
};

module.exports = enrStatsRoutes;