/**
 * Applications router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/routes/application
 */

var apps = require('../controllers/application');
var env = require('../config/dev_env');
var utils = require('../config/utils');
var sec = require('../config/security');

var appRoutes = function(app){

    app.route(env.versionUrl+'admin/applications')
        .get([sec.grantAccess],apps.getAll)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'admin/application/:appId')
        .get([sec.grantAccess],apps.getById)
        .put([sec.grantAccess],apps.update)
        .delete([sec.grantAccess],apps.delete)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'admin/application')
        .post([sec.grantAccess],apps.createOne)
        .all(utils.handleNotAllowed);
};

module.exports = appRoutes;