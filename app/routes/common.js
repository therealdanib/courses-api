/**
 * Common utilities router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/routes/common
 */

var env = require('../config/dev_env');
var utils = require('../config/utils');
var sec = require('../config/security');

var commonRoutes = function(app){

    app.route(env.versionUrl+':objModel/preview/:comId/:modelId')
        .put([sec.grantAccess],utils.updatePreview)
        .all(utils.handleNotAllowed);
};

module.exports = commonRoutes;