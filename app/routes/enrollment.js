/**
 * Enrollments router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/routes/enrollment
 */

var enrollments = require('../controllers/enrollment');
var env = require('../config/dev_env');
var utils = require('../config/utils');
var sec = require('../config/security');

var enrollmentRoutes= function(app){

    app.route(env.versionUrl+'enrollment')
        .post([sec.grantAccess],enrollments.createOne)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollments')
        .post([sec.grantAccess],enrollments.createBulk)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollments/:_companyId')
        .get([sec.grantAccess],enrollments.getByCompany)
        .delete([sec.grantAccess],enrollments.deleteAll)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollment/:_id')
        .get([sec.grantAccess],enrollments.getOne)
        .put([sec.grantAccess],enrollments.updateOne)
        .delete([sec.grantAccess],enrollments.deleteOne)
        .all([sec.grantAccess],utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollment/add:elemType/:enrollment')
        .put([sec.grantAccess],enrollments.addElement)
        .all([sec.grantAccess],utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollment/byUser/:_companyId/:_userId')
        .get([sec.grantAccess],enrollments.getByUser)
        .put([sec.grantAccess],enrollments.updateOne)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollments/byCourse/:companyId/:courseId')
        .get([sec.grantAccess],enrollments.getByCourse)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollments/byPath/:companyId/:pathId')
        .get([sec.grantAccess],enrollments.getByPath)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'enrollment/updateMetrics/:enrId')
        .put([sec.grantAccess],enrollments.updateMetrics)
        .all(utils.handleNotAllowed);
};

module.exports = enrollmentRoutes;