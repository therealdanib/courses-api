/**
 * Session types router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 1.0.0
 * @module app/routes/sessionType
 */

var sessionTypes = require('../controllers/sessionType');
var env = require('../config/dev_env');
var utils = require('../config/utils');
var sec = require('../config/security');

var sessiontypeRoutes= function(app){

    app.route(env.versionUrl+'sessionTypes')
        .get([sec.grantAccess],sessionTypes.getAll)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'sessionType')
        .post([sec.grantAccess],sessionTypes.createOne)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'sessionType/:sessionTypeId')
        .get([sec.grantAccess],sessionTypes.getOne)
        .put([sec.grantAccess],sessionTypes.updateOne)
        .delete([sec.grantAccess],sessionTypes.deleteOne)
        .all(utils.handleNotAllowed);
};

module.exports = sessiontypeRoutes;