/**
 * Courses router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/routes/course
 */

var courses = require('../controllers/course');
var env = require('../config/dev_env');
var utils = require('../config/utils');
var sec = require('../config/security');

var courseRoutes = function(app){

    app.route(env.versionUrl+'courses/:companyId')
        .get([sec.grantAccess],courses.getByCompany)
        .delete([sec.grantAccess],courses.deleteByCompany)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'course/:companyId')
        .post([sec.grantAccess],courses.createOne)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'course/:companyId/:courseId')
        .get([sec.grantAccess],courses.getOne)
        .put([sec.grantAccess],courses.updateOne)
        .delete([sec.grantAccess],courses.deleteOne)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'course/addSessions/:companyId/:courseId')
        .put([sec.grantAccess],courses.addSessions)
        .all(utils.handleNotAllowed);
};

module.exports = courseRoutes;