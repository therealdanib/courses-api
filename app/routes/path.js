/**
 * Paths router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/routes/path
 */

var paths = require('../controllers/path');
var env = require('../config/dev_env');
var utils = require('../config/utils');
var sec = require('../config/security');

var pathRoutes = function(app){

    app.route(env.versionUrl+'paths/:company')
        .get([sec.grantAccess],paths.getAll)
        .delete([sec.grantAccess],paths.deleteByCompany)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'path/:company')
        .post([sec.grantAccess],paths.createOne)
        .all(utils.handleNotAllowed);

    app.route(env.versionUrl+'path/:company/:pathId')
        .get([sec.grantAccess],paths.getOne)
        .put([sec.grantAccess],paths.updateOne)
        .delete([sec.grantAccess],paths.deleteOne)
        .all(utils.handleNotAllowed);
};

module.exports = pathRoutes;