/**
 * Security and authorization router
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module app/routes/security
 */

var security = require('../config/security');
var utils = require('../config/utils');

var securityRoutes = function(app){

    app.route('/getAuthorization')
        .post(security.getCredentials)
        .all(utils.handleNotAllowed);
};

module.exports = securityRoutes;