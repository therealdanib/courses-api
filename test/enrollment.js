'use strict';

var server = require('../app/server');
var chai = require('chai');
var request = require('request');
var async = require('async');
var env = require('../app/config/dev_env');
var token;
var appId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var appPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var demoCompany = "564df8eb86b676540408cf5a";
var demoUser = "564df8eb86b676540408ca8f";
var demoUser2 = "564df8eb86b676540408ca9a";
var demoSessionType = "574df8eb86b676540408cf5f";
var demoCourse;
var demoSingleCourse;
var demoPath;
var demoEnrollment;
var sessionDemo;
var courseDemoId;
var pathDemoId;
var singleCourseDemoId;
var singleCourseSessionId;

describe('Enrollment unit tests', function(){

    before(function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
                    method: 'POST',
                    body: JSON.stringify({
                        "applicationId": appId,
                        "applicationPassword": appPasswd
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    token = obj.token;

                    if(!err){

                        callback();
                    }
                });
            },
            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo course for enrollment unit tests",
                        "goals": ["To prove this app works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"]
                    })
                }, function (err, res, body) {

                    if(!err){
                        var obj = JSON.parse(body);
                        demoCourse = obj.id;
                        callback();
                    }
                });
            },
            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo single course for enrollment unit tests",
                        "goals": [
                            "To prove enrollments work with courses that are not stored into a path!",
                            "To play and have fun with this API!"
                        ],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "sessions": [
                            {
                                "name": "My First Apithy Session",
                                "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                                "sessionType": demoSessionType,
                                "order": 1
                            }
                        ]
                    })
                }, function (err, res, body) {

                    if(!err){
                        var obj = JSON.parse(body);
                        demoSingleCourse = obj.id;
                        callback();
                    }
                });
            },
            function(callback){
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo path for this module unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "courses": [{

                            "courseId": demoCourse,
                            "order": 1
                        }]
                    })
                }, function (err,res,body) {

                    if(!err){

                        var obj = JSON.parse(body);
                        demoPath = obj.id;
                        callback();
                    }
                });
            }

        ], function(err){

            done();
        });
    });

    after(function(done){

        async.series([

            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
                    method: 'DELETE'

                }, function(err) {

                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse,
                    method: 'DELETE'

                }, function(err) {

                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoSingleCourse,
                    method: 'DELETE'

                }, function(err) {

                    callback();
                });
            }
        ], function(err){

            done();
        });
    });

    it('Must return 200 when creating enrollment properly', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
            method: 'POST',
            body: JSON.stringify({
                "_userId": demoUser,
                "_companyId": demoCompany,
                "paths": [
                    {
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "courseOrder": 1,
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sesionOrder": 1
                                    },
                                    {
                                        "sesionOrder": 2
                                    }
                                ]
                            }
                        ]
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(200);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.not.equal('undefined');

            demoEnrollment= obj.id;
            done();
        });
    });

    it('Must return 400 when creating enrollment with an empty body', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
            method: 'POST',
            body: JSON.stringify({})
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('No parameters have been provided');
            done();
        });
    });

    it('Must return 400 when creating enrollment with a wrong user id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
            method: 'POST',
            body: JSON.stringify({
                "_userId": "jdsfalmdfaslkdmasdklas",
                "_companyId": demoCompany,
                "paths": [{
                    "_pathId": demoPath,
                    "courses": [
                        {
                            "_courseId": demoCourse,
                            "sessions": [
                                {
                                    "sessionOrder": 1
                                }
                            ]
                        }
                    ]
                }]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('ValidationError');
            done();
        });

    });

    it('Must return 400 when creating enrollment through a non existing courseId', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
            method: 'POST',
            body: JSON.stringify({
                "_userId": demoUser2,
                "_companyId": demoCompany,
                "courses": [
                    {
                        "_courseId": demoUser,
                        "sessions": [
                            {
                                "sessionOrder": 1
                            }
                        ]
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.not.equal('undefined');
            done();
        });
    });

    it('Must return 400 when creating enrollment through a non existing pathId', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
            method: 'POST',
            body: JSON.stringify({
                "_userId": demoUser2,
                "_companyId": demoCompany,
                "paths":[{
                    "_pathId": demoUser,
                    "courses": [
                        {
                            "_courseId": demoCourse,
                            "sessions": [
                                {
                                    "sessionOrder": 1
                                }
                            ]
                        }
                    ]
                }]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.not.equal('undefined');
            done();
        });
    });

    it('Must return 200 when getting an enrollment properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.include.keys('_id');
            chai.expect(obj._id).to.equal(demoEnrollment);
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting an enrollment by a wrong id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/.sakdsaksda921',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 200 when getting an enrollment by user properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/byUser/'+demoCompany+'/'+demoUser,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.include.keys('_id');
            chai.expect(obj._id).to.equal(demoEnrollment);
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting an enrollment by a wrong user id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/byUser/'+demoCompany+'/ndaskdjnsajda',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting an enrollment by a non existing id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoPath,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });

    });

    it('Must return 404 when getting a path by a non registered user id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/byUser/'+demoCompany+'/'+demoCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 200 when getting all enrollments by course properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/byCourse/'+demoCompany+'/'+demoCourse,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('array');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting all enrollments by a wrong course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/byCourse/'+demoCompany+'/ajdsndasndjaks',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting all enrollments by a non existing course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/byCourse/'+demoCompany+'/'+demoCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });


    it('Must return 200 when getting all enrollments by path properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/byPath/'+demoCompany+'/'+demoPath,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('array');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting all enrollments by a wrong path id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/byPath/'+demoCompany+'/ajdsndasndjaks',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting all enrollments by a non existing path id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/byPath/'+demoCompany+'/'+demoCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 200 when updating an enrollment properly', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
            method: 'PUT',
            body: JSON.stringify({

                "courses": [
                    {
                        "_courseId": demoCourse,
                        "sessions": [
                            {
                                "sessionOrder": 1
                            }
                        ]
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(200);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.equal(demoEnrollment);

            done();
        });
    });

    it('Must return 400 when updating an enrollment by a wrong id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/fsfsdffdsfdsdfs/',
            method: 'PUT',
            body: JSON.stringify({
                "courses": [
                    {
                        "_courseId": demoCourse,
                        "sessions": [
                            {
                                "sessionOrder": 1
                            }
                        ]
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 400 when updating an enrollment passing the enrollment date', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
            method: 'PUT',
            body: JSON.stringify({
                "dateEnrolled": new Date(),
                "courses": [
                    {
                        "_courseId": demoCourse,
                        "sessions": [
                            {
                                "sessionOrder": 1
                            }
                        ]
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);
            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('Some parameters you provided cannot be updated');
            done();
        });
    });

    it('Must return 400 when updating a enrollment passing the user id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
            method: 'PUT',
            body: JSON.stringify({

                "_userId": demoCompany,
                "courses": [
                    {
                        "_courseId": demoCourse,
                        "sessions": [
                            {
                                "sessionOrder": 1
                            }
                        ]
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);
            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('Some parameters you provided cannot be updated');
            done();
        });
    });

    it('Must return 404 when updating an enrollment by a non existing id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoCompany,
            method: 'PUT',
            body: JSON.stringify({

                "courses": [
                    {
                        "_courseId": demoCourse,
                        "sessions": [
                            {
                                "sessionOrder": 1
                            }
                        ]
                    }
                ]
            })

        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 400 when deleting an enrollment by a wrong id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/dsjhdahdhjafdsfsdfdsfsdfdfdf',
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when deleting an enrollment by a non existing enrollment id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoPath,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 200 when deleting an enrollment successfully', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.equal(demoEnrollment);
            done();
        });
    });

    //Bulk insert tests

    it('Must perform a bulk insert properly and throw a 200 ok response', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments',
            method: 'POST',
            body: JSON.stringify([
                {
                    "_userId": "564df8eb86b676540408ca0a",
                    "_companyId": demoCompany,
                    "paths":[{
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "courseOrder": 1,
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    }],
                    "courses": [
                        {
                            "_courseId": demoSingleCourse
                        }
                    ]
                },
                {
                    "_userId": "564df8eb86b676540408ca0b",
                    "_companyId": demoCompany,
                    "paths":[{
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "courseOrder": 1,
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    }],
                    "courses": [
                        {
                            "_courseId": demoSingleCourse
                        }
                    ]
                }
            ])
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(200);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Bulk operation performed successfully');

            done();
        });
    });

    it('Must return 200 when getting all enrollments by company properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/'+demoCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('array');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting all enrollments by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/ajdsndasndjaks',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting all enrollments by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/'+demoPath,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must not perform a bulk insert because users already have an enrollment document', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments',
            method: 'POST',
            body: JSON.stringify([
                {
                    "_userId": demoUser,
                    "_companyId": demoCompany,
                    "paths":[{
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    }],
                    "courses": [
                        {
                            "_courseId": demoSingleCourse
                        }
                    ]
                },
                {
                    "_userId": "564df8eb86b676540408ca0b",
                    "_companyId": demoCompany,
                    "paths":[{
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    }],
                    "courses": [
                        {
                            "_courseId": demoSingleCourse
                        }
                    ]
                }
            ])
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');

            done();
        });
    });

    it('Must not perform a bulk insert because there is nothing to be inserted', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments',
            method: 'POST',
            body: JSON.stringify([])
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('No parameters have been provided');

            done();
        });
    });

    //Bulk delete tests

    it('Must return 200 when deleting all company\'s enrollments successfully', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/'+demoCompany,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Bulk operation performed successfully');
            done();
        });
    });

    it('Must return 400 when passing a wrong company id to delete', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/jdsajkdjsakdjas',
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must add courses and paths to an enrollment object properly', function(done){

        async.series([

            function(callback){

                //Re-creates object

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
                    method: 'POST',
                    body: JSON.stringify({
                        "_userId": demoUser,
                        "_companyId": demoCompany
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    demoEnrollment= obj.id;
                    callback();
                });
            },
            function(callback){

                //Adds course properly

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/addCourse/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({
                        "_courseId": demoSingleCourse
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');
                    callback();
                });

            },
            function(callback){

                //Adds path properly

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/addPath/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "courseOrder": 1,
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);


                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    callback();
                });
            },
            function(callback){

                //Verifies that stats have been calculated

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    sessionDemo = obj.paths[0].courses[0].sessions[0]._id;
                    courseDemoId = obj.paths[0].courses[0]._id;
                    pathDemoId = obj.paths[0]._id;
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.stats.courses.avgProgress).to.equal(0);
                    chai.expect(obj.stats.courses.avgScore).to.equal(0);
                    chai.expect(obj.stats.paths.avgProgress).to.equal(0);
                    chai.expect(obj.stats.paths.avgScore).to.equal(0);
                    callback();
                });
            },
            function(callback){

                //Adds path/course improperly

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/addPath/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({})
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(400);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(400);
                    chai.expect(obj.error).to.equal('Missing parameters');

                    callback();
                });
            },
            function(callback){

                //Trying to add path/course by a wrong id

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/addPath/'+"jdsajkdjsakdjas",
                    method: 'PUT',
                    body: JSON.stringify({
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "courseOrder": 1,
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(400);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('_id');
                    chai.expect(obj.status).to.equal(400);
                    chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
                    chai.expect(obj.error.name).to.equal('CastError');

                    callback();
                });
            },
            function(callback){

                //Trying to add path/course by a non-existing id

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/addPath/'+demoCompany,
                    method: 'PUT',
                    body: JSON.stringify({
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(404);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('_id');
                    chai.expect(obj.status).to.equal(404);
                    chai.expect(obj.message).to.equal('Resource not found');
                    chai.expect(obj.error).to.equal('Nothing found');

                    callback();
                });
            },
            function(callback){

                //Trying to add path/course by Wrong path

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/addNoitem/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({
                        "_pathId": demoPath,
                        "courses": [
                            {
                                "_courseId": demoCourse,
                                "sessions": [
                                    {
                                        "sessionOrder": 1
                                    }
                                ]
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(400);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('_id');
                    chai.expect(obj.status).to.equal(400);
                    chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
                    chai.expect(obj.error).to.equal('Unknown item type');
                    callback();
                });
            },
            function(callback){

                //Finally, it deletes everything

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/'+demoCompany,
                    method: 'DELETE'
                }, function(err,response,body) {

                    var obj = JSON.parse(body);
                    callback();
                });
            }
        ], function(err){

            done();
        });
    });

    it('Must validate every aspect of scores calculation', function(done){

        async.series([

            function(callback){

                //Re-creates object

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
                    method: 'POST',
                    body: JSON.stringify({
                        "_userId": demoUser,
                        "_companyId": demoCompany,
                        "courses": [{
                            "_courseId": demoSingleCourse,
                            "sessions": [
                                {
                                    "sessionOrder": 1
                                }
                            ]
                        }],
                        "paths": [{
                            "_pathId": demoPath,
                            "courses": [
                                {
                                    "courseOrder": 1,
                                    "_courseId": demoCourse,
                                    "sessions": [
                                        {
                                            "sessionOrder": 1
                                        }
                                    ]
                                }
                            ]
                        }],
                        "stats":{
                            "courses":{
                                'avgScore':0,
                                'avgProgress':0
                            },
                            "paths":{
                                'avgScore':0,
                                'avgProgress':0
                            }
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    console.log(body);

                    demoEnrollment= obj.id;
                    callback();
                });
            },
            function(callback){

                //Gets subdocuments identifiers

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    sessionDemo = obj.paths[0].courses[0].sessions[0]._id;
                    courseDemoId = obj.paths[0].courses[0]._id;
                    pathDemoId = obj.paths[0]._id;
                    singleCourseSessionId =obj.courses[0].sessions[0]._id;
                    singleCourseDemoId=obj.courses[0]._id;
                    callback();
                });
            },
            function(callback){

                //Updates stats successfully
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/updateMetrics/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({

                        "path": {
                            "_id":pathDemoId
                        },
                        "course": {
                            "_id":courseDemoId,
                            "progress": 50,
                            "score": 50,
                            "value": 100
                        },
                        "session":{
                            "_id": sessionDemo,
                            "progress": 50,
                            "score": 50,
                            "value": 100
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    callback();
                });
            },
            function(callback){

                //Validates that path, course and session were not marked as finished
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.paths[0].finished).to.equal(false);
                    callback();
                });
            },

            function(callback){

                //Won't find enrollment to update
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/updateMetrics/'+demoCompany,
                    method: 'PUT',
                    body: JSON.stringify({

                        "path": {
                            "_id":pathDemoId
                        },
                        "course": {
                            "_id":courseDemoId,
                            "progress": 50,
                            "score": 50,
                            "value": 100
                        },
                        "session":{
                            "_id": sessionDemo,
                            "progress": 50,
                            "score": 50,
                            "value": 100
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(404);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(404);
                    chai.expect(obj.message).to.equal('Resource not found');

                    callback();
                });
            },

            function(callback){

                //Updates stats successfully
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/updateMetrics/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({

                        "path": {
                            "_id":pathDemoId
                        },
                        "course": {
                            "_id":courseDemoId,
                            "progress": 50,
                            "score": 50,
                            "value": 100
                        },
                        "session":{
                            "_id": sessionDemo,
                            "progress": 50,
                            "score": 50,
                            "value": 100
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    callback();
                });
            },

            function(callback){

                //Validates that path, course and session were marked as finished
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.paths[0].finished).to.equal(true);
                    chai.expect(obj.paths[0].courses[0].finished).to.equal(true);
                    callback();
                });
            },
            function (callback) {

                //Updates stats from single course
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/updateMetrics/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({

                        "course": {
                            "_id":singleCourseDemoId,
                            "progress": 100,
                            "score": 0,
                            "value": 0
                        },
                        "session":{
                            "_id": singleCourseSessionId,
                            "progress": 100,
                            "score": 100,
                            "value": 50
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    callback();
                });
            },
            function (callback) {

                //Does not update stats from single course because they are incomplete
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/updateMetrics/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({

                        "course": {
                            "progress": 100
                        },
                        "session":{
                            "_id": singleCourseSessionId
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(400);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(400);
                    chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
                    chai.expect(obj.error).to.equal('No parameters have been provided');

                    callback();
                });
            },
            function (callback) {

                //Does not update stats from single course because they are incomplete
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/updateMetrics/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({
                        "course": {
                            "progress": 100
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(400);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(400);
                    chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
                    chai.expect(obj.error).to.equal('No parameters have been provided');

                    callback();
                });
            },
            function (callback) {

                //Does not update stats from single course because they are incomplete
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/updateMetrics/'+demoEnrollment,
                    method: 'PUT',
                    body: JSON.stringify({
                        "session":{
                            "_id": singleCourseSessionId,
                            "progress": 100,
                            "score": 100,
                            "value": 50
                        }
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(400);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(400);
                    chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
                    chai.expect(obj.error).to.equal('No parameters have been provided');

                    callback();
                });
            },

            function(callback){

                //Validates that course and session were not marked as finished
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+demoEnrollment,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.courses[0].finished).to.equal(false);
                    callback();
                });
            },

            function(callback){

                //Finally, it deletes everything

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/'+demoCompany,
                    method: 'DELETE'
                }, function(err,response,body) {

                    var obj = JSON.parse(body);
                    callback();
                });
            }
        ], function(err){

            done();
        });
    });
    
    
});