'use strict';

var server = require('../app/server');
var chai = require('chai');
var request = require('request');
var async = require('async');
var env = require('../app/config/dev_env');
var token;
var appId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var appPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var demoCompany = "564df8eb86b676540408cf5a";
var utils = require('../app/config/utils');
var demoCourse;
var demoPath;
var b64Example = 'SGVsbG8sIGkgdXNlZCB0byBiZSBhIGJhc2U2NCBzdHJpbmc=';

describe('Common utilities unit tests', function() {

    before(function (done) {

        async.series([

            function (callback) {

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:' + env.port + '/getAuthorization',
                    method: 'POST',
                    body: JSON.stringify({
                        "applicationId": appId,
                        "applicationPassword": appPasswd
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    token = obj.token;

                    if (!err) {

                        callback();
                    }
                });
            }], function (err) {

            done();
        });
    });

    it('Must return an average of values given', function(done){

        var course = {
            "sessions" : [
                {
                    "sessionProgress" : 30,
                    "sessionScore" : 0
                },
                {
                    "sessionProgress" : 50,
                    "sessionScore" : 0
                }
            ]
        };

        chai.expect(utils.getAvg([],'any')).to.equal(0);
        chai.expect(utils.getAvg(course.sessions,'sessionProgress')).to.equal(40);
        done();
    });

    it('Must decode a base64 string', function(done){

        chai.expect(utils.base64Decode(b64Example)).to.equal("Hello, i used to be a base64 string");
        chai.expect(utils.base64Decode('')).to.equal('');
        chai.expect(utils.base64Decode(undefined)).to.equal('');
        done();
    });

    it('Must return 200 ok when updating preview correctly',function(done){

        async.series([

            //First, we create some course
            function(callback){
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo course for course unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    demoCourse= obj.id;
                    callback();
                });
            },

            //First, we create some path

            function(callback){
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo path for unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    demoPath= obj.id;
                    callback();
                });
            },

            //It Won't find course
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/preview/'+demoCompany+'/'+demoCompany,
                    method: 'PUT',
                    body: JSON.stringify({
                        "format": "image",
                        "url": "http://blablablalba.com/e2892893289"
                    })
                }, function(err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(404);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(404);
                    chai.expect(obj.error).to.equal('Nothing found');
                    chai.expect(obj.message).to.equal('Resource not found');
                    callback();
                });
            },

            //It Won't find collection to look into
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'mycol/preview/'+demoCompany+'/'+demoCourse,
                    method: 'PUT',
                    body: JSON.stringify({
                        "format": "image",
                        "url": "http://blablablalba.com/e2892893289"
                    })
                }, function(err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(404);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(404);
                    chai.expect(obj.error).to.equal('Model not found');
                    chai.expect(obj.message).to.equal('Resource not found');
                    callback();
                });
            },

            //Testing it updates preview correctly
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/preview/'+demoCompany+'/'+demoCourse,
                    method: 'PUT',
                    body: JSON.stringify({
                        "format": "image",
                        "url": "http://blablablalba.com/e2892893289"
                    })
                }, function(err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj).to.not.include.keys('error');
                    callback();
                });
            },

            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/preview/'+demoCompany+'/'+demoPath,
                    method: 'PUT',
                    body: JSON.stringify({
                        "format": "image",
                        "url": "http://blablablalba.com/e2892893289"
                    })
                }, function(err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj).to.not.include.keys('error');
                    callback();
                });
            },

            //Deletes course
            function(callback){
                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'courses/'+demoCompany,
                    method: 'DELETE'

                }, function(err,response,body) {

                    callback();
                });
            },

            //Deletes path
            function(callback){
                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'paths/'+demoCompany,
                    method: 'DELETE'

                }, function(err,response,body) {

                    callback();
                });
            }
        ], function(err){

            if(!err){

                done();
            }
        });
    });
});