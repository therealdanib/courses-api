'use strict';

var server = require('../app/server');
var chai = require('chai');
var request = require('request');
var async = require('async');
var env = require('../app/config/dev_env');
var token;
var appId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var appPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var demoCompany = "564df8eb86b676540408cf5a";

describe('Enrollment Stats unit tests', function() {

    before(function (done) {

        async.series([

            function (callback) {

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:' + env.port + '/getAuthorization',
                    method: 'POST',
                    body: JSON.stringify({
                        "applicationId": appId,
                        "applicationPassword": appPasswd
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    token = obj.token;

                    if (!err) {

                        callback();
                    }
                });
            }], function (err) {

                done();
        });
    });

    it('Must return not found 404 when no user is enrolled', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'stats/enrollments/count/byCompany/'+demoCompany,
            method: 'GET'
        }, function (err, res, body) {

            var obj = JSON.parse(body);
            chai.expect(res.statusCode).to.equal(404);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            done();
        });
    });

    it('Must return 200 and stats when users are enrolled', function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments',
                    method: 'POST',
                    body: JSON.stringify([
                        {
                            "_userId": "564df8eb86b676540408ca0a",
                            "_companyId": demoCompany,
                            "enrolled": true
                        },
                        {
                            "_userId": "564df8eb86b676540408ca0b",
                            "_companyId": demoCompany,
                            "enrolled":true
                        }
                    ])
                }, function () {

                    callback();
                });
            },
            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'stats/enrollments/count/byCompany/'+demoCompany,
                    method: 'GET'
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj._id).to.equal(demoCompany);
                    chai.expect(obj['count']).to.equal(2);
                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollments/'+demoCompany,
                    method: 'DELETE'
                }, function(err,response,body) {

                    callback();
                });
            }
        ],function(){
            done();
        });
    });
});