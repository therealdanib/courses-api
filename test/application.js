'use strict';

var server = require('../app/server');
var chai = require('chai');
var request = require('request');
var async = require('async');
var env = require('../app/config/dev_env');

var adminToken;
var testAppId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var testAppPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var demoAppHardId;

var updatedToken;
var applicationDemo = {

    "name":"Test app",
    "description":"Test application",
    "logoApp":"http://demo.com/logo.png",
    "typeApp": "Web",
    "urlApp": "http://demo.log.com"
};

describe('Application tests', function(){

    before(function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
                    method: 'POST',
                    body: JSON.stringify({
                        "applicationId": testAppId,
                        "applicationPassword": testAppPasswd
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    adminToken = obj.token;

                    if(!err){

                        callback();
                    }
                });
            }

        ], function(err){

            done();

        });
    });

    it('Must return 200 ok with id and password when creating application from correct body', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application',
            method: 'POST',
            body: JSON.stringify(applicationDemo)
        }, function(err,response,body) {

            var obj = JSON.parse(body);
            demoAppHardId = obj.id._id;

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj).to.include.keys('id');
            chai.expect(obj.id).to.include.keys('_id');
            chai.expect(obj.id).to.include.keys('password');
            done();
        });
    });

    it('Must return 400 bad request when updating application with an existing public Id', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/'+demoAppHardId,
            method: 'PUT',
            body: JSON.stringify({

                "name":"Test app 3",
                "publicId": testAppId,
                "description":"Test application new",
                "logoApp":"http://demo4.com/logo.png",
                "typeApp": "Web",
                "urlApp": "http://demo.log.com"
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj).to.include.keys('error');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.code).to.equal(11000);

            done();
        });
    });

    it('Must return 400 when application data is incomplete at the moment of insertion', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application',
            method: 'POST',
            body: JSON.stringify({

                "active": true
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);
            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');

            done();
        });

    });


    it('Must return 400 when some application parameter is not updatable', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/'+demoAppHardId,
            method: 'PUT',
            body: JSON.stringify({

                "_id": "56d8b3b20881207da87773cb",
                "publicId": "56be0ec521665eea2b9290a0"
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj).to.include.keys('error');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('Some application fields are not updatable');

            done();
        });
    });

    it('Must return 400 when some application parameter is not accepted when created', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application',
            method: 'POST',
            body: JSON.stringify({

                "publicId":"a2e53ab5-b89b-4b96-b265-a2d225be0cfd",
                "name":"Test app 2",
                "_id":"56df4ad9b22d1aba1e92078e",
                "description":"Test application",
                "logoApp":"http://demo2.com/logo.png",
                "typeApp": "Web",
                "urlApp": "http://demo2.log.com"

            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj).to.include.keys('error');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('Some application fields are not accepted');

            done();
        });
    });

    it('Must return 400 and error stack as error value when application id is not valid in URL', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/jdsajdjksa',
            method: 'GET'

        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj).to.include.keys('error');
            chai.expect(obj.error.name).to.equal('CastError');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');

            done();
        });
    });

    it('Must return 404 and not found when application id does not exist but has a correct structure', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/56df4ad9b22d1aba1e92078e',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj).to.include.keys('error');
            chai.expect(obj.error).to.equal('Nothing found');
            chai.expect(obj.message).to.equal('Resource not found');

            done();
        });
    });

    it('Must return 200 and one application when application id is valid and existing',function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/'+demoAppHardId,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('_id');
            chai.expect(obj._id).to.equal(demoAppHardId);
            chai.expect(obj).to.include.keys('publicId');
            chai.expect(obj).to.include.keys('name');
            chai.expect(obj).to.include.keys('password');
            chai.expect(obj).to.include.keys('active');
            chai.expect(obj).to.include.keys('created');
            done();
        });
    });

    it('Must return 200 and new token when application is updated correctly', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/'+demoAppHardId,
            method: 'PUT',
            body: JSON.stringify({

                "name" : "Renamed app",
                "active": true
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);
            updatedToken = obj.id.newCredentials.token;

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.include.keys('newCredentials');

            done();
        });
    });

    it('Must return useful response with new token generated where application now is an admin', function(done){

        request ({
            headers: {

                'x-access-token': updatedToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/applications',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('array');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');

            done();
        });
    });

    it('Must return 404 when application id does not match any application on deleting', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': adminToken
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/565c9debf399bed013d49b9e',
            method: 'DELETE'
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(404);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj).to.include.keys('error');
            chai.expect(obj.error).to.equal('Nothing found');
            chai.expect(obj.message).to.equal('Resource not found');

            done();

        });
    });

    it('Must return 400 when applicationId does not have correct structure on delete', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': adminToken
            },

            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/balbalblalafd43',
            method: 'DELETE'
        }, function (err, res, body) {

            var obj = JSON.parse(body);
            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.include.keys('status');
            chai.expect(obj).to.include.keys('error');
            chai.expect(obj.error.name).to.equal('CastError');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            done();

        });
    });

    it('Must return 200 and when application is deleted correctly', function(done){

        request ({
            headers: {
                'x-access-token': adminToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/'+demoAppHardId,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);
            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');

            done();
        });
    });
});