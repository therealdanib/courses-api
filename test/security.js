'use strict';

var server = require('../app/server.js');
var chai = require('chai');
var request = require('request');
var Application = require('../app/models/application');
var async = require('async');
var env = require('../app/config/dev_env');

var nullToken = '';
var trashedToken = 'hdashjdahjsdhjasd';
var expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJleHBpcmluZyI6IjIwMTYtMDYtMDVUMTg6MDI6NDcuNzc0WiIsImFwcGxpY2F0aW9uSWQiOiJhMmU1M2FiNS1iODliLTRiOTYtYjI2NS1hMmQyMjViZTBjZmQifQ.PiUJIdLNi_5WyGmk1SmLsCJN8xMDv_kjajHr09HPq10bnZhjv1M91PUsVEOZlEqmid2QYJ1nxaA62npje_Tkzg';
var wrongPassword = 'M2E4NWNhNDZhYjMxNjJmMGY5ZDY2M2Y5FAA=';
var testAppId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var testAppHardId;
var testAppPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var clientToken;
var clientDisabledToken;

describe('Security & access control tests',function() {


    before(function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
                    method: 'POST',
                    body:JSON.stringify({

                        "applicationId" : testAppId,
                        "applicationPassword" : testAppPasswd
                    })
                }, function (err, res, body) {

                    if(!err){

                        var obj = JSON.parse(body);
                        clientToken = obj.token;
                        testAppHardId = obj.id;

                        callback();
                    }
                });
            }
        ], function(err){
            done();
        });
    });


    it('Must return 401 no token supplied when no token provided', function(done) {
        request ({
            headers: {
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/applications',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(401);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Authorization failed');
            chai.expect(obj.error).to.equal('Error: No token supplied');

            done();
        });
    });

    it('Must return 401 no token supplied when token is null or an empty string', function(done) {
        request ({
            headers: {
                'x-access-token': nullToken
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/applications',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(401);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Authorization failed');
            chai.expect(obj.error).to.equal('Error: No token supplied');

            done();
        });
    });

    it('Must return 401 Token expired when token is no longer date-valid', function(done) {

        request ({
            headers: {
                'x-access-token': expiredToken
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionTypes',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(401);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Authorization failed');
            chai.expect(obj.error).to.equal('Token has expired');

            done();
        });
    });

    it('Must return 401 application is not active when an application tries to perform some API request and is not active',function(done){

        async.series([

            function(callback){

                request ({
                    headers: {
                        'x-access-token': clientToken,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/application/'+testAppHardId,
                    method: 'PUT',
                    body: JSON.stringify({

                        "active": false
                    })
                }, function(err,response,body) {
                    
                    var obj = JSON.parse(body);
                    clientDisabledToken = obj.id.newCredentials.token;
                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': clientDisabledToken
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionTypes',
                    method: 'GET'

                }, function(err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(401);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj.message).to.equal('Authorization failed');
                    chai.expect(obj.error).to.equal('Application is not active');
                    callback();
                });
            },
            function(callback){

                Application.findByIdAndUpdate({"_id": testAppHardId},{"active": true},function(err){

                    if(err){

                        callback(err);
                    }
                    else{

                        callback();
                    }
                });
            }

        ], function(err){

            done();
        })
    });

    it('Must return 401 Not enough or too many segments when token is trash',function(done){

        request ({
            headers: {
                'x-access-token': trashedToken
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/applications',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(401);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Authorization failed');
            chai.expect(obj.error).to.equal('Error: Not enough or too many segments');

            done();
        });
    });

    it('Must return 404 endpoint not found when endpoint does not exist',function(done){

        request ({
            headers: {
                'x-access-token': clientToken
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'endpointdoesnotexist',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Endpoint not found');

            done();
        });
    });

    it('Must return 400 when JSON body is malformed',function(done){

        request ({
            headers: {
                'content-type': 'application/json',
                'x-access-token': clientToken
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/applications',
            method: 'POST',
            body: '{a"".:.."asdads"}'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.match(/^SyntaxError/);
            done();

        });
    });

    it('Must return 400 when application data is missing to get credentials',function(done){

        request ({
            headers: {
                'content-type': 'application/json',
                'x-access-token': clientToken
            },
            uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
            method: 'POST',
            body: JSON.stringify({})
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('Application data is missing');

            done();
        });
    });

    it('Must return 401 Wrong password when password is incorrect ',function(done){

        request ({
            headers: {
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
            method: 'POST',
            body: JSON.stringify({
                'applicationId' : testAppId,
                'applicationPassword': wrongPassword
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(401);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Authorization failed');
            chai.expect(obj.error).to.equal('Wrong application id or password');

            done();
        });
    });

    it('Must return 200 with token, right expiring date and app id when credentials are correct',function(done){

        request ({
            headers: {
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
            method: 'POST',
            body: JSON.stringify({
                'applicationId' : testAppId,
                'applicationPassword': testAppPasswd
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);
            var expDate = new Date();
            expDate.setDate(expDate.getDate() + 30);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj["token"]).to.be.a('string');
            chai.expect(new Date(obj["expires"])).to.be.at.most(expDate);

            done();
        });
    });

    it('Must return 405 when method is not allowed',function(done){

        request ({
            headers: {
                'x-access-token': clientToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'admin/applications',
            method: 'POST'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(405);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj.message).to.equal('Method not allowed');

            done();
        });
    });

    it('Must return 200 when OPTIONS method is performed',function(done){

        request ({
            headers: {
                'x-access-token': clientToken,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'fnjdjknfdsjk',
            method: 'OPTIONS'
        }, function(err,response,body) {

            chai.expect(response.statusCode).to.equal(200);
            done();
        });
    });
});