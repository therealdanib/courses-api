'use strict';

var server = require('../app/server');
var chai = require('chai');
var request = require('request');
var async = require('async');
var env = require('../app/config/dev_env');
var token;
var appId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var appPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var demoCompany = "564df8eb86b676540408cf5a";
var ghostCompany = "564df8eb86b676540106bf0f";
var demoSessionType = "574df8eb86b676540408cf5f";
var demoPath;
var enrollmentId;
var demoCourse;
var demoCourse2;
var uuid = require('node-uuid');
var sessionid = uuid.v4();
var sessionid2 = uuid.v4();

describe('Paths test', function(){
    
    before(function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
                    method: 'POST',
                    body: JSON.stringify({
                        "applicationId": appId,
                        "applicationPassword": appPasswd
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    token = obj.token;

                    if(!err){

                        callback();
                    }
                });
            },
            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo course for path unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "sessions": [
                            {
                                "sessionType": demoSessionType,
                                "name": "Anything",
                                "order": 1,
                                "sessionId" : sessionid
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    console.log(obj);
                    demoCourse = obj.id;

                    if(!err){

                        callback();
                    }
                });
            }

        ], function(err){

            done();
        });
    });

    after(function(done){

        async.series([

            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse,
                    method: 'DELETE'

                }, function(err) {

                    callback();
                });
            }

        ], function(err){

            done();
        });
    });

    it('Must return 200 when creating path properly and validating that creation date has been assigned correctly', function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo path for this module unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "courses": [{

                            "courseId": demoCourse,
                            "order": 1
                        }]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    demoPath= obj.id;
                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
                    method: 'GET'
                }, function(err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.include.keys('_id');
                    chai.expect(obj._id).to.equal(demoPath);
                    chai.expect(obj).to.include.keys('name');
                    chai.expect(obj.openDate).to.not.equal('undefined');
                    chai.expect(obj).to.not.include.keys('status');
                    chai.expect(obj).to.not.include.keys('message');
                    chai.expect(obj).to.not.include.keys('error');
                    callback();
                });
            }
        ], function(err){

            if(!err){

                done();
            }
        });
    });

    it('Must return 400 when creating path improperly', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany,
            method: 'POST',
            body: JSON.stringify({
                "isPublic": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                "courses": [
                    {
                        "courseId": demoCourse,
                        "order": 1
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.not.equal('undefined');
            done();
        });

    });

    it('Must return 400 when creating path with an empty body', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany,
            method: 'POST',
            body: JSON.stringify({})
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('No parameters have been provided');
            done();
        });

    });

    it('Must return 400 when creating path through a malformed company id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/jkdskjdnasndjksa',
            method: 'POST',
            body: JSON.stringify({
                "name": "Demo course for unit testing",
                "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                "isPublic": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                "courses": [
                    {
                        "courseId": demoCourse,
                        "order": 1
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.not.equal('undefined');
            done();
        });
    });

    it('Must return 200 when getting a path properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.include.keys('_id');
            chai.expect(obj._id).to.equal(demoPath);
            chai.expect(obj).to.include.keys('name');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting a path by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/dsjhdahdhja/'+demoPath,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 400 when getting a path by a wrong path id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/fjdkskjfsdjkfkjsd',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting a path by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+ghostCompany+'/'+demoPath,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });

    });

    it('Must return 404 when getting a path by a non existing path id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+ghostCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });

    });

    it('Must return 200 when getting all paths properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'paths/'+demoCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('array');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting all paths by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'paths/ajdsndasndjaks',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting all paths by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'paths/'+ghostCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 200 when updating a path properly', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo path for unit testing updated",
                "goals": ["To play and have fun with this API!"],
                "isPublic": true,
                "validated": true
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(200);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.equal(demoPath);

            done();
        });
    });

    it('Must update existing enrollments when updating a path', function(done){

        async.series([

            function(cb){

                //We enroll someone to this path
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
                    method: 'POST',
                    body: JSON.stringify(
                        {
                            "_userId": "564df8eb86b676540408ca0a",
                            "_companyId": demoCompany,
                            "paths":[{
                                "_pathId": demoPath,
                                "courses": [
                                    {
                                        "courseOrder": 1,
                                        "_courseId": demoCourse,
                                        "sessions": [
                                            {
                                                "sessionOrder": 1,
                                                "sessionId": sessionid,
                                                "sessionScore": 100,
                                                "sessionProgress": 100
                                            }
                                        ],
                                        "courseScore":100,
                                        "courseProgress":100,
                                        "finished":true
                                    }
                                ]
                            }],
                            "stats":{
                                "paths":{
                                    "avgScore": 100,
                                    "avgProgress": 100
                                },
                                "courses":{
                                    "avgScore": 0,
                                    "avgProgress": 0
                                }
                            }
                        }
                    )
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    enrollmentId = obj.id;
                    cb();
                });
            },
            function(cb){

                //We create a new course

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo course 2 for path unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "sessions": [
                            {
                                "order": 1,
                                "sessionId" : sessionid2,
                                "sessionType": demoSessionType,
                                "name": "Anything"
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    demoCourse2 = obj.id;
                    cb();
                });

            },
            function(cb){

                //We add a new course to update the path

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
                    method: 'PUT',
                    body: JSON.stringify({
                        "goals": ["To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "courses": [
                            {
                                "order": 1,
                                "courseId": demoCourse2
                            },
                            {
                                "order": 2,
                                "courseId": demoCourse
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.equal(demoPath);

                    cb();
                });

            },
            function(cb){

                //We validate that enrollment has been modified
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+enrollmentId,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.stats.paths.avgProgress).to.equal(50);
                    chai.expect(obj.stats.paths.avgScore).to.equal(50);
                    chai.expect(obj.paths[0].courses.length).to.equal(2);
                    chai.expect(obj.paths[0].finished).to.equal(false);
                    cb();
                });
            },
            function(cb){

                //Deleting enrollment
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+enrollmentId,
                    method: 'DELETE'

                }, function () {
                    cb();
                });
            },function(cb){

                //Deleting the other course created
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse2,
                    method: 'DELETE'

                }, function () {

                    cb();
                });
            }

        ],function(){

            done();
        });
    });

    it('Must return 400 when updating a path by a wrong company id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/fsfsdffdsfdsdfs/'+demoPath,
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo path for unit testing updated",
                "goals": ["To play and have fun with this API!"],
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                "courses": [
                    {
                        "courseId": demoCourse,
                        "order": 1
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 400 when updating a path passing the creation date', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo path for unit testing updated",
                "goals": ["To play and have fun with this API!"],
                "createDate": "2016-06-07 15:26:32.106Z",
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                "courses": [
                    {
                        "courseId": demoCourse,
                        "order": 1
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);
            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('Cannot update original dates');
            done();
        });
    });

    it('Must return 400 when updating a path by a wrong path id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/nfsdnfdsdjfndks',
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo path for unit testing updated",
                "goals": ["To play and have fun with this API!"],
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                "courses": [
                    {
                        "courseId": demoCourse,
                        "order": 1
                    }
                ]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when updating a path by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+ghostCompany+'/'+demoPath,
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo path for unit testing updated",
                "goals": ["To play and have fun with this API!"],
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                "courses": [
                    {
                        "courseId": demoCourse,
                        "order": 1
                    }
                ]
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 404 when updating a path by a non existing path id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+ghostCompany,
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo path for unit testing updated",
                "goals": ["To play and have fun with this API!"],
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                "courses": [
                    {
                        "courseId": demoCourse,
                        "order": 1
                    }
                ]
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 400 when deleting a path by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/dsjhdahdhja/'+demoPath,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 400 when deleting a path by a wrong path id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/dhbandasjdkfdlsaldsaldsadsa',
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when deleting a path by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+ghostCompany+'/'+demoPath,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 404 when deleting a path by a non existing path id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+ghostCompany,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 200 when deleting a path properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.equal(demoPath);
            done();
        });
    });

    it('Must return 400 when deleting all paths by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'paths/ajdsndasndjaks',
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 200 when deleting all paths properly', function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo path for paths testing updated 2",
                        "goals": ["To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "courses": [
                            {
                                "courseId": demoCourse,
                                "order": 1
                            }
                        ]
                    })
                }, function (err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    demoPath= obj.id;
                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'paths/'+demoCompany,
                    method: 'DELETE'

                }, function(err,response,body) {

                    var obj = JSON.parse(body);
                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    callback();
                });
            }
        ], function(){
            done();
        });
    });
});