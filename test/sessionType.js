'use strict';

var server = require('../app/server');
var chai = require('chai');
var request = require('request');
var async = require('async');
var env = require('../app/config/dev_env');
var token;
var appId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var appPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var demoSTid;


describe('Session Types tests', function(){

    before(function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
                    method: 'POST',
                    body: JSON.stringify({
                        "applicationId": appId,
                        "applicationPassword": appPasswd
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    token = obj.token;

                    if(!err){

                        callback();
                    }
                });
            }

        ], function(err){

            done();
        });
    });

    it('Must return 200 when creating a session type successfully', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType',
            method: 'POST',
            body: JSON.stringify({

                "name": "Demo session type",
                "active": true
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.not.equal('undefined');

            demoSTid = obj.id;
            done();
        });
    });

    it('Must return 400 when creating a session type from a wrong content', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType',
            method: 'POST',
            body: JSON.stringify({
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.not.equal('undefined');
            done();
        });
    });
    
    it('Must return 200 when getting all session types successfully', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionTypes',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('array');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });

    });
    
    it('Must return 200 when getting one session type successfully', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/'+demoSTid,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.include.keys('_id');
            chai.expect(obj).to.include.keys('name');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });
    
    it('Must return 400 and a casting error when getting error on searching session type by a wrong id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/jdfsanjdfadas',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });
    
    it('Must return 404 when getting nothing on searching session type by id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/564df8eb86b676540408cf5a',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });
    
    it('Must return 200 when updating a session type successfully', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/'+demoSTid,
            method: 'PUT',
            body: JSON.stringify({

                "name": "Demo session type updated"
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.equal(demoSTid);

            done();
        });
    });
    
    it('Must return 400 when trying to update a session type from a wrong id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/jdfsanjdfadas',
            method: 'PUT',
            body:JSON.stringify({
                "name": "updated type"
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });
    
    it('Must return 400 when updating a session type from a wrong content', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/'+demoSTid,
            method: 'PUT',
            body: '{a"".:.."asdads"}'

        }, function(err,response,body) {

            var obj = JSON.parse(body);
            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.match(/^SyntaxError/);
            done();

        });
    });
    
    it('Must return 404 when trying to update a session type that does not exist', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/564df8eb86b676540408cf5a',
            method: 'PUT',
            body:JSON.stringify({
                "name": "updated type"
            })
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });
    
    it('Must return 400 when trying to delete a session type from a wrong id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/jdfsanjdfadas',
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });
    
    it('Must return 404 when trying to delete a session type that does not exist', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/564df8eb86b676540408cf5a',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });
    
    it('Must return 200 when deleting an existing session type successfully', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'sessionType/'+demoSTid,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.equal(demoSTid);

            done();
        });
    });

});