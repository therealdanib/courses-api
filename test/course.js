'use strict';

var server = require('../app/server');
var chai = require('chai');
var request = require('request');
var async = require('async');
var env = require('../app/config/dev_env');
var uuid = require('node-uuid');
var token;
var appId = "a2e53ab5-b89b-4b96-b265-a2d225be0cfd";
var appPasswd = "YTY2NjhhOGExOWVkY2E2YmU5NTRlNzYwMTU=";
var demoCompany = "564df8eb86b676540408cf5a";
var ghostCompany = "564df8eb86b676540106bf0f";
var demoSessionType = "574df8eb86b676540408cf5f";
var demoCourse;
var demoPath;
var enrollmentId;
var sessionid = uuid.v4();
var sessionid2 = uuid.v4();

describe('Courses tests', function(){

    before(function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+'/getAuthorization',
                    method: 'POST',
                    body: JSON.stringify({
                        "applicationId": appId,
                        "applicationPassword": appPasswd
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    token = obj.token;

                    if(!err){

                        callback();
                    }
                });
            }
        ], function(){

            done();
        });
    });

    it('Must return 200 when creating course properly and validating that creation date has been assigned correctly', function(done){

        async.series([

            function(callback){
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo course for course unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "sessions":[
                            {
                                "sessionId": sessionid,
                                "order": 1,
                                "name": "Session 1 original",
                                "sessionType": demoSessionType
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    demoCourse= obj.id;
                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse,
                    method: 'GET'
                }, function(err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.include.keys('_id');
                    chai.expect(obj._id).to.equal(demoCourse);
                    chai.expect(obj).to.include.keys('name');
                    chai.expect(obj.openDate).to.not.equal('undefined');
                    chai.expect(obj).to.not.include.keys('status');
                    chai.expect(obj).to.not.include.keys('message');
                    chai.expect(obj).to.not.include.keys('error');
                    callback();
                });
            }
        ], function(err){

            if(!err){

                done();
            }
        });
    });


    it('Must return 400 when creating course improperly', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
            method: 'POST',
            body: JSON.stringify({
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.not.equal('undefined');
            done();
        });

    });

    it('Must return 400 when creating course with an empty body', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
            method: 'POST',
            body: JSON.stringify({})
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('No parameters have been provided');
            done();
        });

    });

    it('Must return 400 when creating course through a malformed company id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/jkdskjdnasndjksa',
            method: 'POST',
            body: JSON.stringify({
                "name": "Demo course for unit testing",
                "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.not.equal('undefined');
            done();
        });
    });

    it('Must return 200 when getting a course properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.include.keys('_id');
            chai.expect(obj._id).to.equal(demoCourse);
            chai.expect(obj).to.include.keys('name');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting a course by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/dsjhdahdhja/'+demoCourse,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 400 when getting a course by a wrong course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/fjdkskjfsdjkfkjsd',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting a course by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+ghostCompany+'/'+demoCourse,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });

    });

    it('Must return 404 when getting a course by a non existing course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+ghostCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });

    });

    it('Must return 200 when getting all courses properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'courses/'+demoCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('array');
            chai.expect(obj).to.not.include.keys('status');
            chai.expect(obj).to.not.include.keys('message');
            chai.expect(obj).to.not.include.keys('error');
            done();
        });
    });

    it('Must return 400 when getting all courses by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'courses/ajdsndasndjaks',
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when getting all courses by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'courses/'+ghostCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must update a course properly and its enrollments', function(done){

        async.series([

            function(cb){
                //We enroll someone to this course
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
                    method: 'POST',
                    body: JSON.stringify(
                        {
                            "_userId": "564df8eb86b676540408ca0a",
                            "_companyId": demoCompany,
                            "courses": [
                                {
                                    "_courseId": demoCourse,
                                    "sessions": [
                                        {
                                            "sessionOrder": 1,
                                            "sessionId": sessionid,
                                            "sessionScore": 100,
                                            "sessionProgress": 100
                                        }
                                    ],
                                    "courseScore":100,
                                    "courseProgress":100,
                                    "finished":true
                                }
                            ],
                            "stats":{
                                "paths":{
                                    "avgScore": 0,
                                    "avgProgress": 0
                                },
                                "courses":{
                                    "avgScore": 100,
                                    "avgProgress": 100
                                }
                            }
                        }
                    )
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    enrollmentId = obj.id;
                    cb();
                });

            },
            function(cb){

                //Update course
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse,
                    method: 'PUT',
                    body: JSON.stringify({
                        "name": "Demo course for course unit testing updated in ut",
                        "goals": ["To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "sessions":[
                            {
                                "sessionId": sessionid,
                                "name":"Session 1",
                                "sessionType": "574df8eb86b676540408cf5f",
                                "order": 1,
                                "value": 33.33,
                                "minTime": 0,
                                "active":true,
                                "edited":true
                            },
                            {
                                "sessionId": sessionid2,
                                "name":"Session 2",
                                "sessionType": "574df8eb86b676540408cf5f",
                                "order": 2,
                                "value": 33.33,
                                "minTime": 0,
                                "active":true
                            },
                            {
                                "name":"Session 3",
                                "sessionType": "574df8eb86b676540408cf5f",
                                "order": 2,
                                "value": 33.33,
                                "minTime": 0,
                                "active":true
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.equal(demoCourse);
                    cb();
                });
            },
            function(cb){
                //Validate enrollment

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+enrollmentId,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.stats.courses.avgProgress).to.equal(0);
                    chai.expect(obj.stats.courses.avgScore).to.equal(0);
                    chai.expect(obj.courses[0].sessions.length).to.equal(3);
                    chai.expect(obj.courses[0].sessions[0].sessionId).to.not.equal(undefined);
                    chai.expect(obj.courses[0].sessions[1].sessionId).to.not.equal(undefined);
                    chai.expect(obj.courses[0].sessions[2].sessionId).to.not.equal(undefined);
                    chai.expect(obj.courses[0].finished).to.equal(false);
                    cb();
                });
            },

            function(cb){

                //Try to update a non-existing course
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCompany,
                    method: 'PUT',
                    body: JSON.stringify({
                        "name": "Demo course for course unit testing updated in ut",
                        "goals": ["To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "sessions":[
                            {
                                "sessionId": sessionid,
                                "name":"Session 1",
                                "sessionType": "574df8eb86b676540408cf5f",
                                "order": 1,
                                "value": 50,
                                "minTime": 0,
                                "active":true,
                                "edited":true
                            },
                            {
                                "sessionId": sessionid2,
                                "name":"Session 2",
                                "sessionType": "574df8eb86b676540408cf5f",
                                "order": 2,
                                "value": 50,
                                "minTime": 0,
                                "active":true
                            }
                        ]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(404);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.status).to.equal(404);
                    chai.expect(obj.message).to.equal('Resource not found');
                    chai.expect(obj.error).to.equal('Nothing found');
                    cb();
                });

            },

            function(cb){
                //Delete enrollment
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+enrollmentId,
                    method: 'DELETE'

                }, function () {
                    cb();
                });
            }

        ],function(){
            done();
        });
    });


    it('Must update course correctly and its enrollments when this one is into a path',function(done){

        async.series([

            function(cb){
                //We create a new path that contains this course

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo path for this module unit tests",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "courses": [{

                            "courseId": demoCourse,
                            "order": 1
                        }]
                    })
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    demoPath= obj.id;
                    cb();
                });
            },
            function(cb){
                //We create an enrollment with this path for any person
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment',
                    method: 'POST',
                    body: JSON.stringify(
                        {
                            "_userId": "564df8eb86b676540408ca0a",
                            "_companyId": demoCompany,
                            "paths":[{
                                "_pathId": demoPath,
                                "courses": [
                                    {
                                        "courseOrder": 1,
                                        "_courseId": demoCourse,
                                        "sessions": [
                                            {
                                                "sessionOrder": 1,
                                                "sessionId": sessionid,
                                                "sessionScore": 100,
                                                "sessionProgress": 100
                                            }
                                        ],
                                        "courseScore":100,
                                        "courseProgress":100,
                                        "finished":true
                                    }
                                ]
                            }],
                            "stats":{
                                "paths":{
                                    "avgScore": 100,
                                    "avgProgress": 100
                                },
                                "courses":{
                                    "avgScore": 0,
                                    "avgProgress": 0
                                }
                            }
                        }
                    )
                }, function (err, res, body) {

                    var obj = JSON.parse(body);
                    enrollmentId = obj.id;
                    cb();
                });

            },
            function(cb){
                //We update the course
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse,
                    method: 'PUT',
                    body: JSON.stringify({
                        "name": "Demo course for course unit testing updated in ut",
                        "goals": ["To play and have fun"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"],
                        "sessions":[
                            {
                                "sessionId": sessionid,
                                "name":"Session 1",
                                "sessionType": "574df8eb86b676540408cf5f",
                                "order": 1,
                                "value": 50,
                                "minTime": 0,
                                "active":true,
                                "edited":true
                            },
                            {
                                "name":"Session 2",
                                "sessionType": "574df8eb86b676540408cf5f",
                                "order": 2,
                                "value": 50,
                                "minTime": 0,
                                "active":true
                            }
                        ]
                    })
                }, function () {
                    cb();
                });
            },
            function(cb){
                //We validate that the enrollment has been updated
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+enrollmentId,
                    method: 'GET'

                }, function (err, res, body) {

                    var obj = JSON.parse(body);

                    chai.expect(res.statusCode).to.equal(200);
                    chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj.stats.paths.avgProgress).to.equal(0);
                    chai.expect(obj.stats.paths.avgScore).to.equal(0);
                    chai.expect(obj.paths[0].courses[0].sessions.length).to.equal(2);
                    chai.expect(obj.paths[0].courses[0].finished).to.equal(false);
                    chai.expect(obj.paths[0].finished).to.equal(false);
                    cb();
                });

            },
            function(cb){
                //We delete the enrollment
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'enrollment/'+enrollmentId,
                    method: 'DELETE'

                }, function () {
                    cb();
                });
            },
            function(cb){
                //We delete the path
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'path/'+demoCompany+'/'+demoPath,
                    method: 'DELETE'

                }, function () {
                    cb();
                });
            }

        ],function(){
            done();
        });
    });

    it('Must return 400 when updating a course by a wrong company id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/fsfsdffdsfdsdfs/'+demoCourse,
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo course for unit testing",
                "goals": ["To play and have fun with this API!"],
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            done();
        });
    });
    it('Must return 400 when updating a course by a wrong course id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/nfsdnfdsdjfndks',
            method: 'PUT',
            body: JSON.stringify({
                "name": "Demo course for unit testing",
                "goals": ["To play and have fun with this API!"],
                "isPublic": true,
                "validated": true,
                "authors": ["564df8eb86b676540408cf5c"]
            })
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            done();
        });
    });
    it('Must return 404 when updating a course by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+ghostCompany+'/'+demoCourse,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 404 when updating a course by a non existing course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+ghostCompany,
            method: 'GET'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    //Begin sessions tests

    it('Must return 200 when adding sessions properly', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/addSessions/'+demoCompany+'/'+demoCourse,
            method: 'PUT',
            body: JSON.stringify([
                {
                    "name": "My First Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 3
                },
                {
                    "name": "My Second Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 4
                }
            ])
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(200);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            done();
        });
    });

    it('Must return 400 when adding sessions by a wrong company id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/addSessions/fsfsdffdsfdsdfs/'+demoCourse,
            method: 'PUT',
            body: JSON.stringify([
                {
                    "name": "My First Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 1
                },
                {
                    "name": "My Second Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 2
                }
            ])
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 400 when adding sessions improperly', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/addSessions/'+demoCompany+'/'+demoCourse,
            method: 'PUT',
            body: JSON.stringify([])
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error).to.equal('Missing parameters');
            done();
        });
    });

    it('Must return 400 when adding sessions by a wrong course id', function(done){

        request({
            headers: {
                'Content-Type': 'application/json',
                'X-Access-Token': token
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/addSessions/'+demoCompany+'/nfsdnfdsdjfndks',
            method: 'PUT',
            body: JSON.stringify([
                {
                    "name": "My First Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 1
                },
                {
                    "name": "My Second Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 2
                }
            ])
        }, function (err, res, body) {

            var obj = JSON.parse(body);

            chai.expect(res.statusCode).to.equal(400);
            chai.expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });
    it('Must return 404 when adding sessions by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/addSessions/'+ghostCompany+'/'+demoCourse,
            method: 'PUT',
            body: JSON.stringify([
                {
                    "name": "My First Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 1
                },
                {
                    "name": "My Second Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 2
                }
            ])
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 404 when adding sessions by a non existing course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/addSessions/'+demoCompany+'/'+ghostCompany,
            method: 'PUT',
            body: JSON.stringify([
                {
                    "name": "My First Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 1
                },
                {
                    "name": "My Second Apithy Session",
                    "s3ContentRef": "http://s3.amazonaws.com/sdakjdsajk/daskdlaksd23392109",
                    "sessionType": demoSessionType,
                    "order": 2
                }
            ])
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    //End sessions tests

    it('Must return 400 when deleting a course by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/dsjhdahdhja/'+demoCourse,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 400 when deleting a course by a wrong course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/dhbandasjdkfdlsaldsaldsadsa',
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 404 when deleting a course by a non existing company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+ghostCompany+'/'+demoCourse,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 404 when deleting a course by a non existing course id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+ghostCompany,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(404);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(404);
            chai.expect(obj.message).to.equal('Resource not found');
            chai.expect(obj.error).to.equal('Nothing found');
            done();
        });
    });

    it('Must return 200 when deleting a course properly', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany+'/'+demoCourse,
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(200);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('error');
            chai.expect(obj.status).to.equal(200);
            chai.expect(obj.message).to.equal('Successfully performed');
            chai.expect(obj.id).to.equal(demoCourse);
            done();
        });
    });

    it('Must return 400 when deleting all courses by a wrong company id', function(done){

        request ({
            headers: {
                'x-access-token': token,
                'content-type': 'application/json'
            },
            uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'courses/ajdsndasndjaks',
            method: 'DELETE'
        }, function(err,response,body) {

            var obj = JSON.parse(body);

            chai.expect(response.statusCode).to.equal(400);
            chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
            chai.expect(obj).to.be.an('object');
            chai.expect(obj).to.not.include.keys('_id');
            chai.expect(obj.status).to.equal(400);
            chai.expect(obj.message).to.equal('Bad Request. Please review API Reference');
            chai.expect(obj.error.name).to.equal('CastError');
            done();
        });
    });

    it('Must return 200 when deleting all courses properly', function(done){

        async.series([

            function(callback){

                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Access-Token': token
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'course/'+demoCompany,
                    method: 'POST',
                    body: JSON.stringify({
                        "name": "Demo course for course unit tests 2",
                        "goals": ["To prove Apithy works!", "To play and have fun with this API!"],
                        "isPublic": true,
                        "validated": true,
                        "authors": ["564df8eb86b676540408cf5c","564df8eb86b676540408cf5d"]
                    })
                }, function (err,response,body) {

                    var obj = JSON.parse(body);

                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    chai.expect(obj.id).to.not.equal('undefined');

                    demoCourse= obj.id;
                    callback();
                });
            },
            function(callback){

                request ({
                    headers: {
                        'x-access-token': token,
                        'content-type': 'application/json'
                    },
                    uri: 'http://127.0.0.1:'+env.port+env.versionUrl+'courses/'+demoCompany,
                    method: 'DELETE'

                }, function(err,response,body) {

                    var obj = JSON.parse(body);
                    chai.expect(response.statusCode).to.equal(200);
                    chai.expect(response.headers['content-type']).to.equal('application/json; charset=utf-8');
                    chai.expect(obj).to.be.an('object');
                    chai.expect(obj).to.not.include.keys('error');
                    chai.expect(obj.status).to.equal(200);
                    chai.expect(obj.message).to.equal('Successfully performed');
                    callback();
                });
            }
        ], function(){
            done();
        });
    });
});