Descripción
===========

### Requerimientos
Esta API REST, esta desarrollada e implementada en las siguientes tecnologís.

Node JS
MongoDB
Express
Mocha / Chai (Para pruebas unitarias)

### Conectividad
Para establecer una comunicación con el API, es necesario proporcionar un identificador de aplicación en formato [UUID][5]
este Identificador deberá ser enviado en conjunto con un Password que le habrá sido proporcionado al equipo de desarrollo 
de la aplicación. Este password, deberá ser envíado codificado en [Base64][6]. 

Una vez validado el UUID y password de la aplicación, se devolvera un Token, mismo que deberá ser incluido en cada 
petición como parte de los headers. Bajo un header personalizado llamado: **X-Access-Token** o **X-Key**.

Todas las peticiones deben de ser enviada con formato application/json.

Por el momento, el API solo recibe y responde las peticiones en formato JSON
