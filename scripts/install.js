/**
 * Install script
 * @author Julio Bravo <julio@geekstadium.com>
 * @version 0.1.0
 * @module scripts/install
 */

'use strict';

var async = require('async');
var mongoose = require('../app/config/mongoose');
var db = mongoose();
var sessionTypesDB= require('../app/models/sessionType');
var appsDB = require('../app/models/application');
var mongo = require('mongoose').mongo;

var sessionTypes = [
    {
        "_id" : mongo.ObjectId("574df8eb86b676540408cf5f"),
        "name": "X-Traceable",
        "active": false
    },
    {
        "_id" : mongo.ObjectId("574df8fa86b676540408cf60"),
        "name": "HTML",
        "active": true
    },
    {
        "_id" : mongo.ObjectId("574df90386b676540408cf61"),
        "name": "Quiz",
        "active": false
    },
    {
        "_id" : mongo.ObjectId("574df92686b676540408cf62"),
        "name": "Evaluation",
        "active": false
    },
    {
        "_id" : mongo.ObjectId("57a9f7276e1171250794ab22"),
        "name": "Slideshow",
        "active": true
    }
];

    var defaultApps = [{
        "_id" : mongo.ObjectId("5755a68ef9112cb27599ed97"),
        "password" : "d8c8aa3810ed239d56fa2baea20d81b0d3d1f4f39275648dd110c0322dbd9ff6",
        "modifyDate" : new Date(),
        "created" : new Date(),
        "publicId" : "a2e53ab5-b89b-4b96-b265-a2d225be0cfd",
        "name" : "Apithy",
        "active" : true,
        "urlApp" : "http://web.apithy.com",
        "typeApp" : "Web",
        "logoApp" : "http://apithy.com/logo.png",
        "description" : "Apithy common access"
    }];

async.series([

    function(callback){

        console.log('Installing default sessionTypes');

        sessionTypesDB.collection.insert(sessionTypes,function(err){

            if(err){

                console.log("There was an error inserting session types (Hint: They might already exist):");
                console.log(err.message+'\n');
                callback();
            }

            else {

                console.log("Session types have been inserted correctly");
                callback();
            }
        });
    },
    function(callback){

        console.log('Installing default applications for the use of this API');
        appsDB.collection.insert(defaultApps, function(err){

            if(err){

                console.log("There was an error inserting default application. (Hint: They might already exist):");
                console.log(err.message+'\n');
                callback();
            }

            else {

                console.log("Default application installed successfully");
                callback();
            }
        });
    }

], function(err){

    process.exit();
});